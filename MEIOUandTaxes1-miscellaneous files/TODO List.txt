
#####################################
### Developers' Index of Progress ###
#####################################

##############
### To Do ####
##############

####################
### Top Priority ###
####################
#
# ---Mining, 60% Complete
#		*Relative intensity for mine deposits need to be assigned so that we can move forward with a balanced mining system. 
#
######################
### Lower Priority ###
######################
#
#
# ---Interaction events, decisions, policites 0% Complete
#		* Right now, the system has no events, decisions, or policies to interact with.  This will be one of the final, but essential, changes.
#	
# ---Urban Production Power, 90% Complete
#		* Urban Production Power needs to properly downgrade when skill is too low in addition to send skill abroad when a city begins to decompose
#
# ---Wealth, 90% Complete
#	
# ---Upper Classes 0% Complete	
#		*Upper classes need to be assigned at game start.  They also need to rise and fall based on wealth and power in specific provinces.
#		
# ---Education, 0% Complete
#		*Need an education system which educates the upper classes and some urban dwellers.  This system will involve university grants, university events, etc.  
#		*Universities will "compete" for prestige and pupils.
#
# ---Localisations, 50% Complete
#		* Missing numberous localisations for modifiers.  Will also need to revise all localisations. 
#  
# ---Create Scripted Effects, 80% Complete
#		* convert a few recurring blocks of code to scripted effects for simplicity
#
# ---Special Mapmode, 30% Complete
#		* flesh out the special mapmode so that it represents all pertinent variables
#
# ---TimeLapse Event, 60% Complete
#		* need a timelapse event for wealth and local AI.
#
# ---Migration, 70 percent complete
#		* need ways to interact with colonialism better so that the player can encourage and discourage migration 
# ---Estate Integration?
# ---Update industrialisation mechanics
# ---Natural disasters
#

#################
### Completed ###
#################

# ---Rural Population Growth 
# ---Urban Population Growth
# ---Ideal Population Growth
# ---Food System
# ---Centers of Production
# ---Centers of Trade
# ---Preeminent Cities
# ---Weather system 
# ---Plague
# ---Famine
# ---Provine Trauma
# ---War damage to populations
# ---Census for assigning Urban Gravity
# ---Census for assigning wealth
