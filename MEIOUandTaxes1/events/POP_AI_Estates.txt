namespace = POP_AI_Estates

### By Demian

#country_event = {  ### Global event to assess whether to request manpower
#	id = POP_AI_Estates.001
#	title = POP_AI_Estates.001.title
#	desc = POP_AI_Estates.001.desc
#	picture = REFORM_eventPicture
#	is_triggered_only = yes
#	hidden = yes
#			
#	immediate = {
#		every_country = {
#			limit = {
#				ai = yes
#			}
#			if = {
#				limit = {
#					NOT = { manpower_percentage = 0.10 }
#				}
#				if = {
#					limit = {
#						NOT = { has_country_modifier = GN_manpower_given }
#						check_variable = { which = estate_greater_nobles_loyalty		value = 0.11 }
#					}
#					set_country_flag = need_GN_manpower
#					country_event = {
#						id = POP_Estates_Interactions.005
#					}
#				}
#				if = {
#					limit = {
#						NOT = { has_country_modifier = LN_manpower_given }
#						check_variable = { which = estate_lesser_nobles_loyalty		value = 0.11 }
#					}
#					set_country_flag = need_LN_manpower
#					country_event = {
#						id = POP_Estates_Interactions.105
#					}
#				}
#			}
#			if = {
#				limit = {
#					NOT = { manpower_percentage = 0.4 }
#				}
#				if = {
#					limit = {
#						NOT = { has_country_modifier = GN_manpower_given }
#						check_variable = { which = estate_greater_nobles_loyalty		value = 0.55 }
#					}
#					set_country_flag = need_GN_manpower
#					country_event = {
#						id = POP_Estates_Interactions.005
#					}
#				}
#				if = {
#					limit = {
#						NOT = { has_country_modifier = LN_manpower_given }
#						check_variable = { which = estate_lesser_nobles_loyalty		value = 0.55 }
#					}
#					set_country_flag = need_LN_manpower
#					country_event = {
#						id = POP_Estates_Interactions.105
#					}
#				}
#			}
#			if = {
#				limit = {
#					is_at_war = yes
#					OR = {
#						NOT = { war_score = -10 }
#						any_enemy_country = {
#							war_with = PREV
#						}
#						any_rival_country = {
#							war_with = PREV
#						}
#					}
#				}
#				if = {
#					limit = {
#						NOT = { has_country_flag = GN_partial_support }
#						check_variable = { which = estate_greater_nobles_loyalty		value = 0.4 }
#					}
#					set_country_flag = GN_raise_some_levies
#					country_event = {
#						id = POP_Estates_Interactions.007
#						days = 10
#					}
#				}
#				if = {
#					limit = {
#						NOT = { has_country_flag = LN_partial_support }
#						check_variable = { which = estate_lesser_nobles_loyalty		value = 0.4 }
#					}
#					set_country_flag = LN_raise_some_levies
#					country_event = {
#						id = POP_Estates_Interactions.108
#						days = 10
#					}
#				}
#			}
#		}
#	}
#			
#	option = {
#		name = "POP_AI_Estates.001.a"
#	}
#}
#
#country_event = {  ### Actual estate war aid ai loop
#	id = POP_AI_Estates.001
#	title = POP_AI_Estates.001.title
#	desc = POP_AI_Estates.001.desc
#	picture = REFORM_eventPicture
#	is_triggered_only = yes
#	hidden = no
#	
#	trigger = {
#	}
#		
#	immediate = {
#		if = { limit = { is_at_war = no }
#			hidden_effect = {
#				country_ai_clear_all_estate_war_aid = yes
#			}
#			custom_tooltip = war_aid_ai_ending_tooltip
#		}
#		if = { limit = { is_at_war = yes }
#			hidden_effect = {
#				country_ai_evaluate_estate_war_aid = yes
#			}
#			clr_country_flag = country_ai_request_war_aid_GNobles
#			clr_country_flag = country_ai_request_war_aid_LNobles
#			clr_country_flag = country_ai_request_war_aid_Burghers
#			if = { limit = { check_variable = { which = country_ai_war_strain 	which = country_ai_estate_war_aid_GNobles } }
#				set_country_flag = country_ai_request_war_aid_GNobles
#				custom_tooltip = war_aid_ai_GNobles_tooltip
#			}
#			if = { limit = { check_variable = { which = country_ai_war_strain 	which = country_ai_estate_war_aid_LNobles } }
#				set_country_flag = country_ai_request_war_aid_LNobles
#				custom_tooltip = war_aid_ai_LNobles_tooltip
#			}
#			if = { limit = { check_variable = { which = country_ai_war_strain 	which = country_ai_estate_war_aid_Burghers } }
#				set_country_flag = country_ai_request_war_aid_Burghers
#				custom_tooltip = war_aid_ai_Burghers_tooltip
#			}
#			country_event = { id = POP_AI_Estates.001 days = 90 }
#		}
#	}
#			
#	option = {
#		name = "POP_AI_Estates.001.a"
#	}
#}

