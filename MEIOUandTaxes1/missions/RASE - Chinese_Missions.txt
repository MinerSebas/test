# Chinese Missions

annex_dai_viet = {
	
	type = country
	
	category = MIL
	
	target_provinces = {
		owned_by = DAI
	}

	allow = {
		faction_in_power = temples
		has_country_flag = mandate_of_heaven_claimed
		is_free_or_tributary_trigger = yes
		mil = 4
		is_lesser_in_union = no
		exists = DAI
		NOT = { alliance_with = DAI }
		NOT = { has_country_modifier = military_victory }
		DAI = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			religion_group = ROOT
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			NOT = { has_country_flag = mandate_of_heaven_claimed }
			NOT = { faction_in_power = temples }
			NOT = { exists = DAI }
			is_subject_other_than_tributary_trigger = yes
			is_lesser_in_union = yes
			DAI = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		faction_in_power = temples
		NOT = { exists = DAI }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = DAI value = 0 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			if = {
				limit = {
					NOT = {
						is_permanent_claim = ROOT
					}
				}
				remove_claim = ROOT
			}
		}
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


colonize_taiwan = {
	
	type = country
	
	category = DIP
	
	allow = {
		OR = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
		}
		OR = {
			has_factions = no
			faction_in_power = enuchs
		}
		owns = 695						# Nanjing
		2305 = { is_empty = yes base_tax = 1 }
		num_of_colonists = 1
		num_of_ports = 1
		NOT = { has_country_modifier = colonial_enthusiasm }
	}
	abort = {
		OR = {
			AND = {
				NOT = { faction_in_power = enuchs }
				has_factions = yes
			}
			AND = {
				2305 = { is_empty = no }
				NOT = { owns = 2305 } # Hoanya
			}
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		owns = 2305 # Hoanya
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
	}
	effect = {
		add_prestige = 3
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


colonize_deren = {
	
	type = country
	
	category = DIP
	
	allow = {
		culture_group = tungusic
		1044 = {
			is_empty = yes
			base_tax = 1
			has_discovered = ROOT
		}
		num_of_colonists = 1
		num_of_ports = 1
		NOT = { has_country_modifier = colonial_enthusiasm }
	}
	abort = {
		OR = {
			NOT = {
				OR = {
					has_factions = no
					faction_in_power = enuchs
				}
			}
			AND = {
				1044 = { is_empty = no }
				NOT = { owns = 1044 }
			}
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		owns = 1044 # Ulia
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
	}
	effect = {
		add_prestige = 3
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


china_discovers_india = {
	
	type = country
	
	category = DIP
	
	allow = {
		OR = {
			culture_group = chinese_group
			has_country_flag = mandate_of_heaven_claimed
		}
		has_idea = quest_for_the_new_world
		NOT = { indian_coast_group = { has_discovered = ROOT } }
		num_of_ports = 1
	}
	abort = {
		NOT = { has_idea = quest_for_the_new_world }
		NOT = { num_of_ports = 1 }
	}
	success = {
		indian_coast_group = { has_discovered = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = land_of_opportunity
		}
		modifier = {
			factor = 2
			has_idea = colonial_ventures
		}
	}
	effect = {
		add_treasury = 50
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


start_the_conquest_of_ming_china = {
	
	type = country
	
	category = MIL
	
	allow = {
		culture_group = tungusic
		exists = MNG
		NOT = { war_with = MNG }
		NOT = { owns = 720 }		# Onggirat
		NOT = { owns = 2248 }		# Liaoxi
		NOT = { owns = 2249 }		# Liaodong
		is_lesser_in_union = no
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = military_victory }
	}
	abort = {
		OR = {
			NOT = { exists = MNG }
			is_lesser_in_union = yes
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		NOT = { war_with = MNG }
		OR = {
			owns = 720 # Xilin Gol
			owns = 2248 # Josotu
			owns = 2249 # Fengtian
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		add_claim = 720 # Xilin Gol
		add_claim = 2248 # Josotu
		add_claim = 2249
	}
	abort_effect = {
		remove_claim = 720 # Xilin Gol
		remove_claim = 2248 # Josotu
		remove_claim = 2249 # Fengtian
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		720 = {
			add_territorial_core_effect = yes
		}
		2248 = {
			add_territorial_core_effect = yes
		}
		2249 = {
			add_territorial_core_effect = yes
		}
	}
}

