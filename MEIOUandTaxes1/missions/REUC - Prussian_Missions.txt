# Prussia Missions

conquer_silesia = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_areas_list = {
		lower_silesia_area
		upper_silesia_area
	}
	
	target_provinces = {
		NOT = { owned_by = ROOT }
	}
	allow = {
		OR = {
			tag = BRA
			tag = PRU
		}
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_modifier = prussian_ambition }
		OR = {
			NOT = {
				lower_silesia_area = {
					type = all
					owned_by = ROOT
				}
			}
			NOT = {
				upper_silesia_area = {
					type = all
					owned_by = ROOT
				}
			}
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 6
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		set_country_flag = conquered_silesia
		add_country_modifier = {
			name = "prussian_ambition"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

connect_brandenburg_and_prussia = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces_list = {
		48
		2355
	}
	
	target_provinces = {
		NOT = { owned_by = ROOT }
	}

	allow = {
		normal_or_historical_nations = yes
		tag = BRA
		exists = PRU
		is_free_or_tributary_trigger = yes
		PRU = { vassal_of = BRA }
		NOT = { is_neighbor_of = PRU }
		NOT = { has_country_modifier = prussian_ambition }
		NOT = { has_country_flag = connect_brandenburg_and_prussia }
		OR = {
			48 = {		# Stolp
				NOT = { owned_by = ROOT }
				NOT = { owned_by = PRU }
				any_neighbor_province = { owned_by = ROOT }
				OR = {
					any_neighbor_province = { owned_by = PRU }
					2355 = {
						any_neighbor_province = {
							owned_by = PRU
						}
					}
				}
			}
			2355 = {		# Danzig
				NOT = { owned_by = ROOT }
				NOT = { owned_by = PRU }
				any_neighbor_province = { owned_by = PRU }
				OR = {
					any_neighbor_province = { owned_by = ROOT }
					2849 = {
						any_neighbor_province = {
							owned_by = ROOT
						}
					}
				}
			}
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = PRU }
			war_with = PRU
		}
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 3000
		modifier = {
			factor = 2
			has_opinion = { who = PRU value = 100 }
		}
		modifier = {
			factor = 2
			adm = 4
		}
		modifier = {
			factor = 2
			alliance_with = PRU
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		set_country_flag = connect_brandenburg_and_prussia
		add_country_modifier = {
			name = "prussian_ambition"
			duration = 3650
		}
	}
}

connect_the_prussian_lands = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_areas_list = {
		west_prussia_area
	}
	
	target_provinces = {
		NOT = { owned_by = ROOT }
	}
	allow = {
		tag = PRU
		owns = 50			# Berlin
		owns = 41			# Königsberg
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_modifier = prussian_ambition }
		NOT = {
			prussia_region = {
				type = all
				owned_by = ROOT
			}
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 6
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 20
		add_prestige = 5
		add_country_modifier = {
			name = "prussian_ambition"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

conquer_swedish_pomerania = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		OR = {
			tag = BRA
			tag = PRU
		}
		exists = SWE
		is_free_or_tributary_trigger = yes
		mil = 4
		SWE = { owns = 47 }
		47 = { any_neighbor_province = { owned_by = ROOT } }		# Vorpommern
		NOT = { has_opinion = { who = SWE value = 50 } }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 47 # Vorpommern
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			SWE = { is_at_war = yes }
		}
	}
	immediate = {
		add_claim = 47 # Vorpommern
	}
	abort_effect = {
		remove_claim = 47 # Vorpommern
	}
	effect = {
		add_prestige = 5
		47 = {
			add_territorial_core_effect = yes
		}
	}
}

conquer_hinterpommern = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_areas_list = {
		hinterpommern_area
	}
	
	target_provinces = {
		NOT = { owned_by = ROOT }
	}
	
	allow = {
		OR = {
			tag = BRA
			tag = PRU
		}
		NOT = { hinterpommern_area = { type = all country_or_vassal_holds = ROOT } }
		is_free_or_tributary_trigger = yes
		mil = 4
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1500
		modifier = {
			factor = 2
			mil = 6
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

brandenburg_prussia_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = BRA
		exists = PRU
		NOT = { war_with = PRU }
		NOT = { has_opinion = { who = PRU value = 100 } }
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
		NOT = { has_country_modifier = foreign_contacts }
		mil = 4
	}
	abort = {
		OR = {
			NOT = { exists = PRU }
			war_with = PRU
		}
	}
	success = {
		PRU = { has_opinion = { who = BRA value = 150 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = PRU value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = PRU value = -100 } }
		}
	}
	effect = {
		add_stability_1 = yes
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 3650
		}
	}
}

prussia_brandenburg_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = PRU
		exists = BRA
		mil = 4
		NOT = { war_with = BRA }
		NOT = { has_opinion = { who = BRA value = 100 } }
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
		NOT = { has_country_modifier = foreign_contacts }
	}
	abort = {
		OR = {
			NOT = { exists = BRA }
			war_with = BRA
		}
	}
	success = {
		BRA = { has_opinion = { who = PRU value = 150 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = BRA value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = BRA value = -100 } }
		}
	}
	effect = {
		add_stability_1 = yes
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 3650
		}
	}
}

vassalize_prussia = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = BRA
		exists = PRU
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = {
			has_country_modifier = military_vassalization
		}
		PRU = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = PRU }
			PRU = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		PRU = { vassal_of = BRA }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = PRU value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = PRU
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = PRU
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = PRU
			}
		}
	}
}

annex_prussia = {
	
	type = country
	
	category = DIP
	
	target_provinces = {
		owned_by = PRU
	}
	allow = {
		tag = BRA
		exists = PRU
		is_free_or_tributary_trigger = yes
		mil = 4
		PRU = {
			vassal_of = BRA
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			NOT = { exists = PRU }
			PRU = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = PRU }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_opinion = { who = PRU value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = PRU value = 200 }
		}
	}
	immediate = {
		every_target_province = {
			add_core = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_core = ROOT
		}
	}
	effect = {
		add_prestige = 10
		hidden_effect = {
			every_target_province = {
				remove_core = ROOT
			}
		}
	}
}

brandenburg_breaks_free_from_poland = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = BRA
		exists = POL
		vassal_of = POL
		mil = 4
		NOT = {
			has_country_modifier = prussian_ambition
		}
		NOT = { has_country_flag = free_of_poland_bra }
	}
	abort = {
		NOT = { exists = POL }
	}
	success = {
		NOT = { vassal_of = POL }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = POL value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = POL value = -50 } }
		}
	}
	effect = {
		add_prestige = 10
		add_army_tradition = 30
		set_country_flag = free_of_poland_bra
		add_country_modifier = {
			name = "prussian_ambition"
			duration = 3650
		}
	}
}

prussia_breaks_free_from_poland = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = PRU
		exists = POL
		vassal_of = POL
		mil = 4
		NOT = {
			has_country_modifier = prussian_ambition
		}
		NOT = { has_country_flag = free_of_poland_pru }
	}
	abort = {
		NOT = { exists = POL }
	}
	success = {
		NOT = { vassal_of = POL }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = POL value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = POL value = -50 } }
		}
	}
	effect = {
		add_prestige = 10
		add_army_tradition = 30
		set_country_flag = free_of_poland_pru
		add_country_modifier = {
			name = "prussian_ambition"
			duration = 3650
		}
	}
}

prussia_partitions_poland = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = PRU
		is_year = 1700
		exists = POL
		exists = HAB
		exists = RUS
		is_free_or_tributary_trigger = yes
		is_lesser_in_union = no
		mil = 4
		NOT = { has_country_modifier = polish_partitions }
		NOT = { has_country_flag = partitioned_poland }
		POL = {
			owns = 43 # Pomorsko
			owns = 254 # Nakelskie
			owns = 255 # Kujawy
			owns = 256 # Mazowieckie
			owns = 257 # Plockie
			owns = 1531 # Biala
			owns = 2399 # Kaliskie
			owns = 2400 # Leczyckie
			owns = 2401 # Poznanskie
			is_neighbor_of = PRU
			is_neighbor_of = RUS
			is_neighbor_of = HAB
		}
		OR = {
			is_core = 43 # Pomorsko
			is_core = 254 # Nakelskie
			is_core = 255 # Kujawy
			is_core = 256 # Mazowieckie
			is_core = 257 # Plockie
			is_core = 1531 # Biala
			is_core = 2399 # Kaliskie
			is_core = 2400 # Leczyckie
			is_core = 2401 # Poznanskie
		}
		has_opinion = { who = RUS value = 0 }
		has_opinion = { who = HAB value = 0 }
	}
	abort = {
		NOT = { exists = POL }
	}
	success = {
		OR = {
			owns = 43 # Pomorsko
			owns = 254 # Nakelskie
			owns = 255 # Kujawy
			owns = 256 # Mazowieckie
			owns = 257 # Plockie
			owns = 1531 # Biala
			owns = 2399 # Kaliskie
			owns = 2400 # Leczyckie
			owns = 2401 # Poznanskie
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			POL = { NOT = { num_of_cities = ROOT } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = POL value = 0 } }
		}
	}
	effect = {
		add_prestige = 10
		set_country_flag = partitioned_poland
		add_country_modifier = {
			name = "polish_partitions"
			duration = 3650
		}
	}
}

conquer_ratibor = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		OR = {
			tag = BRA
			tag = PRU
		}
		is_free_or_tributary_trigger = yes
		mil = 4
		is_year = 1700
		1278 = { # Opole
			NOT = { owned_by = ROOT }
			any_neighbor_province = { owned_by = ROOT }
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 1278 # Opole
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 6
		}
	}
	immediate = {
		add_claim = 1278 # Opole
	}
	abort_effect = {
		remove_claim = 1278 # Opole
	}
	effect = {
		add_army_tradition = 10
		1278 = {
			add_territorial_core_effect = yes
		}
	}
}

subjugate_westphalia = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = PRU
		exists = WES
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { westphalian_circle_region = { owned_by = ROOT } }
		WES = { is_neighbor_of = ROOT }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		westphalian_circle_region = { owned_by = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 6
		}
	}
	immediate = {
		westphalian_circle_region = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		westphalian_circle_region = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}
