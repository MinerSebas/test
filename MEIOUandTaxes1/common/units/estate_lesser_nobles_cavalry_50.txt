# Western Greater Nobles 50

type = cavalry
unit_type = western
maneuver = 2

offensive_morale = 7
defensive_morale = 6
offensive_fire = 3
defensive_fire = 3
offensive_shock = 6
defensive_shock = 6

trigger = {
	always = no
}