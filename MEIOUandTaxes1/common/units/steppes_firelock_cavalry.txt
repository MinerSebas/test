#Firelock Cavalry (20)

type = cavalry
unit_type = steppestech
maneuver = 2

offensive_morale = 4
defensive_morale = 2
offensive_fire = 3
defensive_fire = 1
offensive_shock = 3
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}