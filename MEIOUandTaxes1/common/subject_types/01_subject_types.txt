# To use a created subject type in history you need to use this structure
#dependency = {
#	subject_type = "[subject type]"
#	first = [Overlord tag]
#	second = [Subject tag]
#	start_date = YYYY.M.DD
#	end_date = YYYY.M.DD
#}

protectorate = {
	copy_from = vassal
	
	# Graphics:
	sprite = GFX_icon_march
	diplomacy_overlord_sprite = GFX_diplomacy_leadmarch
	diplomacy_subject_sprite = GFX_diplomacy_weakmarch
	
	# Properties:
	pays_overlord = 0.5
	military_focus = 1.5
	base_liberty_desire = -5.0
	can_be_annexed = no
	
	# Subject Interactions:
	scutage = no
	subsidize_armies = yes
	#fortify_march = yes
	
	send_officers = yes
	
	# Modifiers:
	modifier_overlord = clear #We don't want to duplicate the ones inherited from vassal
	modifier_subject = clear #Ditto
	modifier_subject = {
		modifier = subject_nation
	}
	modifier_overlord = {
		modifier = march_subject
	}
	modifier_subject = {
		modifier = march_bonus
		trigger = {
			NOT = {
				total_development = 200
			}
		}
		expiration_message_overlord = MARCHTOOLARGE
		expiration_message_subject = MARCHTOOLARGEUS
	}
}

appanage_subject = {
	copy_from = vassal
	count = vassal
	
	is_potential_overlord = {
		technology_group = western
	}
	
	can_be_established = {
		technology_group = western
	}
	
	# Properties:
	can_be_annexed = no
	takes_diplo_slot = no
	gets_help_with_rebels = yes
	separatists_become_subjects = yes
	allows_taking_land_without_independence = yes
	can_use_claims = yes
	pays_overlord = 0.5
	forcelimit_bonus = 0
	
	trust_on_start = 10
	
	military_focus = 0.8
	
	relative_power_class = 0
	
	can_fight = {
		same_overlord = appanage_subject
	}
	can_rival = {
		same_overlord = appanage_subject
	}
	can_ally = {
		same_overlord = appanage_subject
	}
	can_marry = {
		same_overlord = appanage_subject
	}
	
	# Subject Interactions
	#(disable)
	scutage = no
	grant_core_claim = yes
	grant_province = yes
	place_relative_on_throne = no
	enforce_religion = no
	enforce_culture = no
	seize_territory = no
	
	# Modifiers
	
	modifier_overlord = clear #We don't want to duplicate the ones inherited from vassal
	modifier_subject = clear #Ditto
	modifier_subject = {
		modifier = vassal_nation
	}
	modifier_overlord = {
		modifier = vassal_subject
	}
	
	overlord_opinion_modifier = is_vassal
	subject_opinion_modifier = is_vassal
}

decentralized_vassal = {
	copy_from = appanage_subject
	count = vassal
	is_potential_overlord = {
		technology_group = eastern
	}
	
	can_be_established = {
		technology_group = eastern
	}
	
	# Properties:
	can_be_annexed = no
	gets_help_with_rebels = yes
	separatists_become_subjects = yes
	allows_taking_land_without_independence = no
	can_have_subjects_of_other_types = yes
	overlord_can_attack = no
	overlord_protects_external = yes
	takes_diplo_slot = no
	can_use_claims = yes
	joins_overlords_wars = yes
	
	pays_overlord = 0.5
	forcelimit_bonus = 1.25
	
	trust_on_start = 10
	
	military_focus = 1.0
	
	relative_power_class = 0
	
	can_fight = {
	}
	can_rival = {
		
	}
	can_ally = {
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	can_marry = {
		independent_nations = yes
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	
	# Subject Interactions
	#(disable)
	scutage = no
	grant_core_claim = yes
	grant_province = yes
	place_relative_on_throne = no
	enforce_religion = no
	enforce_culture = no
	seize_territory = no
	
	# Modifiers
	
	modifier_overlord = clear #We don't want to duplicate the ones inherited from vassal
	modifier_subject = clear #Ditto
	modifier_subject = {
		modifier = vassal_nation
	}
	modifier_overlord = {
		modifier = vassal_subject
	}
	
	overlord_opinion_modifier = is_vassal
	subject_opinion_modifier = is_vassal
}



rebellious_decentralized_vassal = {
	copy_from = appanage_subject
	count = vassal
	is_potential_overlord = {
		technology_group = eastern
	}
	
	can_be_established = {
		technology_group = eastern
	}
	
	# Properties:
	can_be_annexed = no
	gets_help_with_rebels = no
	separatists_become_subjects = yes
	allows_taking_land_without_independence = yes
	can_have_subjects_of_other_types = yes
	overlord_can_attack = yes
	overlord_protects_external = yes
	takes_diplo_slot = no
	can_use_claims = no
	joins_overlords_wars = no
	pays_overlord = 0
	forcelimit_bonus = 0
	
	trust_on_start = 10
	
	military_focus = 1.2
	
	relative_power_class = 1
	
	can_fight = {
		independent_nations = yes
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	can_rival = {
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	can_ally = {
		independent_nations = yes
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	can_marry = {
		independent_nations = yes
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	
	# Subject Interactions
	#(disable)
	scutage = no
	grant_core_claim = no
	grant_province = no
	place_relative_on_throne = no
	enforce_religion = no
	enforce_culture = no
	seize_territory = no
	
	# Modifiers
	
	modifier_overlord = clear #We don't want to duplicate the ones inherited from vassal
	modifier_subject = clear #Ditto
	modifier_subject = {
		modifier = vassal_nation
	}
	modifier_overlord = {
		modifier = vassal_subject
	}
	
	overlord_opinion_modifier = is_vassal
	subject_opinion_modifier = is_vassal
}

subjugated_vassal = {
	copy_from = appanage_subject
	count = vassal
	is_potential_overlord = {
		technology_group = east_african
	}
	
	can_be_established = {
		technology_group = east_african
	}
	
	# Properties:
	can_be_annexed = no
	can_fight_independence_war = yes
	gets_help_with_rebels = yes
	separatists_become_subjects = yes
	allows_taking_land_without_independence = no
	can_have_subjects_of_other_types = yes
	overlord_can_attack = no
	overlord_protects_external = yes
	takes_diplo_slot = no
	can_use_claims = yes
	joins_overlords_wars = yes
	pays_overlord = 0.5
	forcelimit_bonus = 0.5
	
	trust_on_start = 10
	
	military_focus = 1.0
	
	relative_power_class = 0
	
	can_fight = {
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	can_rival = {
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	can_ally = {
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	can_marry = {
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	
	# Subject Interactions
	#(disable)
	scutage = no
	grant_core_claim = yes
	grant_province = yes
	place_relative_on_throne = no
	enforce_religion = no
	enforce_culture = no
	seize_territory = no
	
	# Modifiers
	
	modifier_overlord = clear #We don't want to duplicate the ones inherited from vassal
	modifier_subject = clear #Ditto
	modifier_subject = {
		modifier = vassal_nation
	}
	modifier_overlord = {
		modifier = vassal_subject
	}
	
	overlord_opinion_modifier = is_vassal
	subject_opinion_modifier = is_vassal
}

rebellious_subjugated_vassal = {
	copy_from = appanage_subject
	count = vassal
	is_potential_overlord = {
		technology_group = east_african
	}
	
	can_be_established = {
		technology_group = east_african
	}
	
	# Properties:
	can_be_annexed = no
	can_fight_independence_war = yes
	gets_help_with_rebels = yes
	separatists_become_subjects = yes
	allows_taking_land_without_independence = no
	can_have_subjects_of_other_types = yes
	overlord_can_attack = yes
	overlord_protects_external = yes
	takes_diplo_slot = no
	can_use_claims = yes
	joins_overlords_wars = no
	pays_overlord = 0
	forcelimit_bonus = 0
	
	trust_on_start = 10
	
	military_focus = 1.2
	
	relative_power_class = 1
	
	can_fight = {
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
		independent_nations = yes
		other_overlord = all
	}
	can_rival = {
		independent_nations = yes
		other_overlord = all
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	can_ally = {
		other_overlord = all
		independent_nations = yes
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	can_marry = {
		other_overlord = all
		independent_nations = yes
		same_overlord = decentralized_vassal
		same_overlord = rebellious_decentralized_vassal
	}
	
	# Subject Interactions
	#(disable)
	scutage = no
	grant_core_claim = yes
	grant_province = yes
	place_relative_on_throne = no
	enforce_religion = no
	enforce_culture = no
	seize_territory = no
	
	# Modifiers
	
	modifier_overlord = clear #We don't want to duplicate the ones inherited from vassal
	modifier_subject = clear #Ditto
	modifier_subject = {
		modifier = vassal_nation
	}
	modifier_overlord = {
		modifier = vassal_subject
	}
	
	overlord_opinion_modifier = is_vassal
	subject_opinion_modifier = is_vassal
}

close_tributary_state = {
	copy_from = tributary_state
	count = tributary_state
	
	# Triggers:
	is_potential_overlord = {
		has_dlc = "Mandate of Heaven"
		NOT = {
			is_subject_of_type = tributary_state
			is_subject_of_type = close_tributary_state
		}
		OR = {
			religion_group = taoic	#Changed to DG 'Eastern' equivalent
		}
	}
	can_be_established = {
		has_dlc = "Mandate of Heaven"
		NOT = { has_country_flag = barbarian_claimant_china }
		NOT = { has_country_flag = mandate_of_heaven_claimed }
		NOT = { has_country_flag = red_turban_reb }
		NOT = { has_country_modifier = mongol_xingsheng }
	}
	
	overlord_protects_external = yes
	counts_for_borders = yes
	transfer_trade_power = yes
	
	placate_rulers = yes
	# (continuous)
	embargo_rivals = yes
	siphon_income = yes
	
	liberty_desire_development_ratio = 0.087
}