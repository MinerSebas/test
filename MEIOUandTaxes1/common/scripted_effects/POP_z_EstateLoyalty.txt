# 5 years worth of variables are stored, from which average is drawn
EstateLoyalty_CalcVarMain = {
	set_variable = { which = estate_tempvar which = estate_$estate$_$type$ }
	divide_variable = { which = estate_tempvar value = 5 }
	
	# Initialize the variables if none
	# If not, do the usual
	if = {
		limit = {
			is_variable_equal = { which = estate_$estate$_$type$_avg value = 0 }
		}
		if = {
			limit = {
				NOT = { is_variable_equal = { which = estate_tempvar value = 0 } }
			}
			set_variable = { which = estate_$estate$_$type$_1 which = estate_tempvar }
			set_variable = { which = estate_$estate$_$type$_2 which = estate_tempvar }
			set_variable = { which = estate_$estate$_$type$_3 which = estate_tempvar }
			set_variable = { which = estate_$estate$_$type$_4 which = estate_tempvar }
			set_variable = { which = estate_$estate$_$type$_5 which = estate_tempvar }
			
			set_variable = { which = estate_$estate$_$type$_avg which = estate_tempvar }
			multiply_variable = { which = estate_$estate$_$type$_avg value = 5 }
			
			set_variable = { which = estate_$estate$_$type$_old which = estate_$estate$_$type$_avg }
		}
	}
	else = {
		set_variable = { which = estate_$estate$_$type$_old which = estate_$estate$_$type$_avg }
		
		subtract_variable = { which = estate_$estate$_$type$_avg which = estate_$estate$_$type$_5 }
		change_variable = { which = estate_$estate$_$type$_avg which = estate_tempvar }
		
		set_variable = { which = estate_$estate$_$type$_5 which = estate_$estate$_$type$_4 }
		set_variable = { which = estate_$estate$_$type$_4 which = estate_$estate$_$type$_3 }
		set_variable = { which = estate_$estate$_$type$_3 which = estate_$estate$_$type$_2 }
		set_variable = { which = estate_$estate$_$type$_2 which = estate_$estate$_$type$_1 }
		set_variable = { which = estate_$estate$_$type$_1 which = estate_tempvar }
	}
	
	set_variable = { which = estate_tempvar value = 0 }
}

EstateLoyalty_CalcVar = {
	EstateLoyalty_CalcVarMain = { estate=$estate$ type=weight_share }
	EstateLoyalty_CalcVarMain = { estate=$estate$ type=treasury_annual }
}


EstateLoyalty_MergeHelper = {
	set_country_flag = NameCheck_$type$
	
	set_variable = { which = estate_$estate$_$type$_avg_modified which = estate_$estate$_$type$_avg }
	set_variable = { which = estate_$estate$_$type$_old_modified which = estate_$estate$_$type$_old }
	
	if = {
		limit = {
			has_country_flag = NameCheck_weight_share
		}
		set_variable = { which = estate_$estate$_$type$_average value = 1 }
		change_variable = { which = estate_$estate$_$type$_average which = estate_influencechange_$estate$_averageadd }
		
		change_variable = { which = estate_$estate$_$type$_old_modified which = estate_$estate$_$type$_old_merge }
		divide_variable = { which = estate_$estate$_$type$_old_modified which = estate_$estate$_$type$_average }
		
		set_variable = { which = estate_$estate$_$type$_average value = 0 }
	}
	
	clr_country_flag = NameCheck_$type$
}

EstateLoyalty_CalcShiftModify = {
	subtract_variable = { which = estate_$estate$_change which = estate_$estate$_changefactor_5 }
	
	set_variable = { which = estate_$estate$_changefactor_5 which = estate_$estate$_changefactor_4 }
	set_variable = { which = estate_$estate$_changefactor_4 which = estate_$estate$_changefactor_3 }
	set_variable = { which = estate_$estate$_changefactor_3 which = estate_$estate$_changefactor_2 }
	set_variable = { which = estate_$estate$_changefactor_2 which = estate_$estate$_changefactor_1 }
	set_variable = { which = estate_$estate$_changefactor_1 which = estate_influence_change }
	
	change_variable = { which = estate_$estate$_change which = estate_$estate$_changefactor_1 }
}

# From the saved variables, calculate loyalty shift
EstateLoyalty_CalcShiftMain = {
	# Consider merge
	EstateLoyalty_MergeHelper = { estate=$estate$ type=$type$ }
	
	# Get initial change value
	set_variable = { which = estate_$type$_change which = estate_$estate$_$type$_avg_modified }
	divide_variable = { which = estate_$type$_change which = estate_$estate$_$type$_old_modified }
	
	subtract_variable = { which = estate_$type$_change value = 1 }
	multiply_variable = { which = estate_$type$_change value = 4 }
	
	# Modify change depending on their absolute values
	set_variable = { which = estate_absolute which = estate_$estate$_$type$_avg_modified }
	subtract_variable = { which = estate_absolute which = estate_$estate$_$type$_old_modified }
	divide_variable = { which = estate_absolute value = $standard$ }
	
	if = {
		limit = {
			check_variable = { which = estate_absolute value = 1 }
		}
		set_variable = { which = estate_absolute value = 1 }
	}
	
	multiply_variable = { which = estate_$type$_change which = estate_absolute }
	
	set_variable = { which = estate_absolute value = 0 }
	
	# Make estate loyalties to 'tick' even to small changes
	set_variable = { which = estate_$type$_change_minor which = estate_$type$_change }
	multiply_variable = { which = estate_$type$_change_minor value = 1.5 }
	
	if = {
		limit = {
			check_variable = { which = estate_$type$_change_minor value = 0 }
		}
		divide_variable = { which = estate_$type$_change_minor value = 1.5 }
		
		if = {
			limit = {
				check_variable = { which = estate_$type$_change_minor value = 0.02 }
			}
			set_variable = { which = estate_$type$_change_minor value = 0.02 }
		}
	}
	else_if = {
		limit = {
			NOT = { check_variable = { which = estate_$type$_change_minor value = -0.02 } }
		}
		set_variable = { which = estate_$type$_change_minor value = -0.02 }
	}
	
	set_variable = { which = EstateLoyalty_steepns value = 0 }
	
	if = {
		limit = {
			check_variable = { which = estate_$type$_change value = 0 }
		}
		if = {
			limit = {
				check_variable = { which = estate_$type$_change value = 3 }
			}
			set_variable = { which = estate_$type$_change value = 0.2 }
		}
		else = {
			set_variable = { which = EstateLoyalty_steepns value = 1.5 }
		}
	}
	else = {
		if = {
			limit = {
				NOT = { check_variable = { which = estate_$type$_change value = -1.5 } }
			}
			set_variable = { which = estate_$type$_change value = -0.2 }
		}
		else = {
			set_variable = { which = EstateLoyalty_steepns value = 3 }
		}
	}
	
	if = {
		limit = {
			NOT = { is_variable_equal = { which = EstateLoyalty_steepns value = 0 } }
		}
		logistic_funct = {
			type1=which type2=value type3=which type4=value
			inp=estate_$type$_change midpnt=0 steepns=EstateLoyalty_steepns maxval=0.4
		}
		subtract_variable = { which = logistic_val value = 0.2 }
		
		set_variable = { which = estate_$type$_change which = logistic_val }
		
		set_variable = { which = logistic_val value = 0 }
		
		set_variable = { which = EstateLoyalty_steepns value = 0 }
	}
	
	change_variable = { which = estate_influence_change which = estate_$type$_change }
	change_variable = { which = estate_influence_change which = estate_$type$_change_minor }
	
	
	set_variable = { which = estate_$type$_change value = 0 }
	set_variable = { which = estate_$type$_change_minor value = 0 }
	
	set_variable = { which = estate_$estate$_$type$_avg_modified value = 0 }
	set_variable = { which = estate_$estate$_$type$_old_modified value = 0 }
	set_variable = { which = estate_$estate$_$type$_avg_merge value = 0 }
	set_variable = { which = estate_$estate$_$type$_old_merge value = 0 }
}

EstateLoyalty_CalcShift = {
	set_variable = { which = estate_influence_change value = 0 }
	
	EstateLoyalty_CalcShiftMain = { estate=$estate$ type=weight_share standard=0.05 }
	EstateLoyalty_CalcShiftMain = { estate=$estate$ type=treasury_annual standard=30 }
	
	divide_variable = { which = estate_influence_change value = 2 }
	
	EstateLoyalty_CalcShiftModify = { estate=$estate$ }
	
	set_variable = { which = estate_influence_divisor which = estate_$estate$_change }
	multiply_variable = { which = estate_influence_divisor value = 8 }
	
	if = {
		limit = {
			NOT = { check_variable = { which = estate_influence_divisor value = 0 } }
		}
		multiply_variable = { which = estate_influence_divisor value = -1 }
	}
	
	if = {
		limit = {
			NOT = { check_variable = { which = estate_influence_divisor value = 1 } }
		}
		set_variable = { which = estate_influence_divisor value = 1 }
	}
	
	divide_variable = { which = estate_influence_change which = estate_influence_divisor }
	divide_variable = { which = estate_influence_change which = estate_influence_divisor }
	
	# Truncate
	divide_variable = { which = estate_influence_change value = 10 }
	multiply_variable = { which = estate_influence_change value = 1000 }
	
	
	# Clean
	set_variable = { which = estate_influencechange_$estate$_averageadd value = 0 }
	set_variable = { which = estate_influence_divisor value = 0 }
}


# Edit loyalty and loyalty change display variables according to the calculated shift
EstateLoyalty_CalcLoyalty = {
	# Display
	subtract_variable = { which = estate_$estate$_influencechange_display which = estate_$estate$_influencechange_display_5 }
	
	set_variable = { which = estate_$estate$_influencechange_display_5 which = estate_$estate$_influencechange_display_4 }
	set_variable = { which = estate_$estate$_influencechange_display_4 which = estate_$estate$_influencechange_display_3 }
	set_variable = { which = estate_$estate$_influencechange_display_3 which = estate_$estate$_influencechange_display_2 }
	set_variable = { which = estate_$estate$_influencechange_display_2 which = estate_$estate$_influencechange_display_1 }
	set_variable = { which = estate_$estate$_influencechange_display_1 which = estate_influence_change }
	
	change_variable = { which = estate_$estate$_influencechange_display which = estate_$estate$_influencechange_display_1 }
	
	# Actual loyalty change
	if = {
		limit = {
			check_variable = { which = estate_influence_change value = 0 }
		}
		set_variable = { which = $estate$_loyalty_added which = estate_influence_change }
		
		estate_$estate$_loyalty_added = yes
	}
	else = {
		multiply_variable = { which = estate_influence_change value = -1 }
		
		set_variable = { which = $estate$_loyalty_removed which = estate_influence_change }
		
		multiply_variable = { which = estate_influence_change value = -1 }
		
		estate_$estate$_loyalty_removed = yes
	}
}


# Merge the conquered provinces
EstateLoyalty_MergeInitMain = {
	set_variable = { which = estate_$estate$_$type$_1 which = PREV }
	set_variable = { which = estate_$estate$_$type$_2 which = PREV }
	set_variable = { which = estate_$estate$_$type$_3 which = PREV }
	set_variable = { which = estate_$estate$_$type$_4 which = PREV }
	set_variable = { which = estate_$estate$_$type$_5 which = PREV }
	
	set_variable = { which = estate_$estate$_$type$_avg which = PREV }
	set_variable = { which = estate_$estate$_$type$_old which = PREV }
}

EstateLoyalty_MergeInit = {
	EstateLoyalty_MergeInitMain = { estate=burghers type=weight_share }
	EstateLoyalty_MergeInitMain = { estate=burghers type=treasury_annual }
	
	EstateLoyalty_MergeInitMain = { estate=tribals type=weight_share }
	EstateLoyalty_MergeInitMain = { estate=tribals type=treasury_annual }
	
	EstateLoyalty_MergeInitMain = { estate=greater_nobles type=weight_share }
	EstateLoyalty_MergeInitMain = { estate=greater_nobles type=treasury_annual }
	
	EstateLoyalty_MergeInitMain = { estate=lesser_nobles type=weight_share }
	EstateLoyalty_MergeInitMain = { estate=lesser_nobles type=treasury_annual }
}

EstateLoyalty_MergeCalcVar = {
	set_variable = { which = estate_$estate$_weight_share which = owner }
	set_variable = { which = estate_$estate$_treasury_annual which = owner }
	
	EstateLoyalty_CalcVarMain = { estate=$estate$ type=weight_share }
	EstateLoyalty_CalcVarMain = { estate=$estate$ type=treasury_annual }
	
	set_variable = { which = estate_$estate$_weight_share value = 0 }
	set_variable = { which = estate_$estate$_treasury_annual value = 0 }
}

EstateLoyalty_MergeSendVarMain = {
	set_variable = { which = estate_$estate$_$type$_avg_merge which = estate_$estate$_$type$_avg }
	set_variable = { which = estate_$estate$_$type$_old_merge which = estate_$estate$_$type$_old }
	
	owner = {
		change_variable = { which = estate_$estate$_$type$_avg_merge which = PREV }
		change_variable = { which = estate_$estate$_$type$_old_merge which = PREV }
	}
	
	set_variable = { which = estate_$estate$_$type$_avg_merge value = 0 }
	set_variable = { which = estate_$estate$_$type$_old_merge value = 0 }
}

EstateLoyalty_MergeSendVar = {
	owner = { change_variable = { which = estate_influencechange_$estate$_num_merge value = 1 } }
		
	EstateLoyalty_MergeSendVarMain = { estate=$estate$ type=weight_share }
	EstateLoyalty_MergeSendVarMain = { estate=$estate$ type=treasury_annual }
}

EstateLoyalty_MergeClearCountryMain = {
	set_variable = { which = estate_$estate$_$type$_avg_merge value = 0 }
	set_variable = { which = estate_$estate$_$type$_old_merge value = 0 }
}

EstateLoyalty_MergeClearCountry = {
	set_variable = { which = estate_influencechange_$estate$_num_merge value = 0 }
	set_variable = { which = estate_influencechange_$estate$_num value = 0 }
	
	EstateLoyalty_MergeClearCountryMain = { estate=$estate$ type=weight_share }
	EstateLoyalty_MergeClearCountryMain = { estate=$estate$ type=treasury_annual }
}

EstateLoyalty_MergeFinishMain = {
	divide_variable = { which = estate_$estate$_$type$_avg_merge which = estate_influencechange_$estate$_num }
	divide_variable = { which = estate_$estate$_$type$_old_merge which = estate_influencechange_$estate$_num }
	
	set_variable = { which = estate_influencechange_$estate$_averageadd which = estate_influencechange_$estate$_num_merge }
	divide_variable = { which = estate_influencechange_$estate$_averageadd which = estate_influencechange_$estate$_num }
}

EstateLoyalty_MergeFinish = {
	EstateLoyalty_MergeFinishMain = { estate=$estate$ type=weight_share }
	EstateLoyalty_MergeFinishMain = { estate=$estate$ type=treasury_annual }
	
	set_variable = { which = estate_influencechange_$estate$_num_merge value = 0 }
	set_variable = { which = estate_influencechange_$estate$_num value = 0 }
}


# Clear the variables
EstateLoyalty_ClearHelper = {
	set_variable = { which = estate_$estate$_$type$_1 value = 0 }
	set_variable = { which = estate_$estate$_$type$_2 value = 0 }
	set_variable = { which = estate_$estate$_$type$_3 value = 0 }
	set_variable = { which = estate_$estate$_$type$_4 value = 0 }
	set_variable = { which = estate_$estate$_$type$_5 value = 0 }
	
	set_variable = { which = estate_$estate$_$type$_avg value = 0 }
	set_variable = { which = estate_$estate$_$type$_old value = 0 }
}

EstateLoyalty_ClearLoyalty = {
	set_variable = { which = loyalty_reset value = 0.5 }
	subtract_variable = { which = loyalty_reset which = estate_$estate$_loyalty }
	
	divide_variable = { which = loyalty_reset value = 10 }
	multiply_variable = { which = loyalty_reset value = 1000 }
	
	if = {
		limit = {
			NOT = { is_variable_equal = { which = loyalty_reset value = 0 } }
		}
		if = {
			limit = {
				is_variable_equal = { which = loyalty_reset value = 0 }
			}
			set_variable = { which = $estate$_loyalty_added which = loyalty_reset }
			
			estate_$estate$_loyalty_added = yes
			
			set_variable = { which = $estate$_loyalty_added value = 0 }
		}
		else = {
			set_variable = { which = $estate$_loyalty_removed which = loyalty_reset }
			multiply_variable = { which = $estate$_loyalty_removed value = -1 }
			
			estate_$estate$_loyalty_removed = yes
			
			set_variable = { which = $estate$_loyalty_removed value = 0 }
		}
		
		set_variable = { which = loyalty_reset value = 0 }
	}
}

EstateLoyalty_Clear = {
	set_variable = { which = estate_$estate$_influencechange_display_1 value = 0 }
	set_variable = { which = estate_$estate$_influencechange_display_2 value = 0 }
	set_variable = { which = estate_$estate$_influencechange_display_3 value = 0 }
	set_variable = { which = estate_$estate$_influencechange_display_4 value = 0 }
	set_variable = { which = estate_$estate$_influencechange_display_5 value = 0 }
	
	set_variable = { which = estate_$estate$_influencechange_display value = 0 }
	
	EstateLoyalty_ClearHelper = { estate=$estate$ type=weight_share }
	EstateLoyalty_ClearHelper = { estate=$estate$ type=treasury_annual }
}