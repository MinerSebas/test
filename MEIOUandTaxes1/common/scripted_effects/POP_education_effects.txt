university_head_hunting_big = {
	set_province_flag = university_head_hunted
	
	set_variable = { which = itr value = 1 }
	
	while = {
		limit = {
			check_variable = { which = itr value = 1 }
			check_variable = { which = university_available_contribution value = 1 }
		}
		
		set_variable = { which = art_power_compare value = 0 }
		
		every_province = {
			limit = {
				OR = {
					culture_group = PREV
					
					is_variable_equal = { which = cont_id which = PREV }
				}
				check_variable = { which = population_needing_education	value = 1 }
				religion_group = PREV
				#	NOT = { has_province_flag = university_present }
			}
			set_variable = { which = art_power_compare which = art_power }
			
			if = {
				limit = {
					PREV = { owner = { has_country_flag = sponsor_domestic_students } }
					
					owned_by = PREV
				}
				multiply_variable = { which = art_power_compare value = 4 }
			}
			
			if = {
				limit = {
					PREV = { check_variable = { which = university_local_contribution value = 1 } }
					
					owned_by = PREV
				}
				change_variable = { which = art_power_compare value = 1 }
				multiply_variable = { which = art_power_compare value = 10 }
			}
			
			if = {
				limit = {
					culture = PREV
				}
				multiply_variable = { which = art_power_compare value = 1.5 }
			}
			if = {
				limit = {
					culture_group = PREV
				}
				multiply_variable = { which = art_power_compare value = 1.25 }
			}
			
			if = {
				limit = {
					religion = PREV
				}
				multiply_variable = { which = art_power_compare value = 1.5 }
			}
			
			if = {
				limit = {
					area = PREV
				}
				multiply_variable = { which = art_power_compare value = 2 }
			}
			if = {
				limit = {
					region = PREV
				}
				multiply_variable = { which = art_power_compare value = 2 }
			}
			if = {
				limit = {
					is_variable_equal = { which = sc_id which = PREV }
				}
				multiply_variable = { which = art_power_compare value = 2 }
			}
			
			if = {
				limit = {
					check_variable = { which = art_power_compare which = PREV }
				}
				set_province_flag = foreign_university_cand
				
				PREV = { set_variable = { which = art_power_compare which = PREV } }
			}
		}
		
		every_province = {
			limit = {
				has_province_flag = foreign_university_cand
			}
			clr_province_flag = foreign_university_cand
			
			if = {
				limit = {
					is_variable_equal = { which = art_power_compare which = PREV }
					
					PREV = { check_variable = { which = university_available_contribution value = 1 } }
				}
				set_global_flag = student_hunt_continue
				
				subtract_variable = { 			which = population_needing_education					value = 1 }
				change_variable = { 			which = university_education_multiplier 				value = 1 }
				PREV = { subtract_variable = {  which = university_available_contribution				value = 1 } }
				
				if = {
					limit = {
						owned_by = PREV
					}
					change_variable = { 			which = university_domestic_student						value = 1 }
					PREV = { subtract_variable = { which = university_local_contribution value = 1 } }
				}
				if = {
					limit = {
						NOT = { owned_by = PREV }
					}
					PREV = { change_variable = { 	which = university_foreign_students						value = 1 } }
					change_variable = { 			which = university_student_sent_abroad 					value = 1 }
				}
				
				# A portion of province's art power is sent to the university
				set_variable = { which = art_power_sent value = 1 }
				
				if = {
					limit = {
						check_variable = { which = upper_class_population value = 1 }
					}
					if = {
						limit = {
							is_variable_equal = {
								which = upper_class_population
								value = 0
							}
						}
						
						# log = "<ERROR><BF596D80><THIS:[This.GetName]><PREV:[Prev.GetName]><ROOT:[Root.GetName]><FROM:[From.GetName]> Division by zero!"
						
					}
					else = {
						divide_variable = {
							which = art_power_sent
							which = upper_class_population
						}
					}
				}
				
				divide_variable = { which = art_power_sent value = 2 }
				multiply_variable = { which = art_power_sent which = art_power }
				
				subtract_variable = { which = art_power which = art_power_sent }
				
				PREV = { change_variable = { which = art_power_sent which = PREV } }
			}
		}
		
		if = {
			limit = {
				NOT = { has_global_flag = student_hunt_continue }
			}
			set_variable = { which = itr value = 0 }
		}
		clr_global_flag = student_hunt_continue
	}
}

university_head_hunting_medium = {
	set_province_flag = university_head_hunted
	
	set_variable = { which = itr value = 1 }
	
	while = {
		limit = {
			check_variable = { which = itr value = 1 }
			check_variable = { which = university_available_contribution value = 1 }
		}
		
		set_variable = { which = art_power_compare value = 0 }
		
		every_province = {
			limit = {
				OR = {
					is_variable_equal = { which = sc_id which = PREV }
					
					culture_group = PREV
				}
				check_variable = { which = population_needing_education	value = 1 }
				religion_group = PREV
				#	NOT = { has_province_flag = university_present }
			}
			set_variable = { which = art_power_compare which = art_power }
			
			if = {
				limit = {
					PREV = { owner = { has_country_flag = sponsor_domestic_students } }
					
					owned_by = PREV
				}
				multiply_variable = { which = art_power_compare value = 4 }
			}
			
			if = {
				limit = {
					PREV = { check_variable = { which = university_local_contribution value = 1 } }
					
					owned_by = PREV
				}
				change_variable = { which = art_power_compare value = 1 }
				multiply_variable = { which = art_power_compare value = 10 }
			}
			
			if = {
				limit = {
					culture = PREV
				}
				multiply_variable = { which = art_power_compare value = 1.5 }
			}
			if = {
				limit = {
					culture_group = PREV
				}
				multiply_variable = { which = art_power_compare value = 1.25 }
			}
			
			if = {
				limit = {
					religion = PREV
				}
				multiply_variable = { which = art_power_compare value = 1.5 }
			}
			
			if = {
				limit = {
					area = PREV
				}
				multiply_variable = { which = art_power_compare value = 2 }
			}
			if = {
				limit = {
					region = PREV
				}
				multiply_variable = { which = art_power_compare value = 2 }
			}
			
			if = {
				limit = {
					check_variable = { which = art_power_compare which = PREV }
				}
				set_province_flag = foreign_university_cand
				
				PREV = { set_variable = { which = art_power_compare which = PREV } }
			}
		}
		
		every_province = {
			limit = {
				has_province_flag = foreign_university_cand
			}
			clr_province_flag = foreign_university_cand
			
			if = {
				limit = {
					is_variable_equal = { which = art_power_compare which = PREV }
					
					PREV = { check_variable = { which = university_available_contribution value = 1 } }
				}
				set_global_flag = student_hunt_continue
				
				subtract_variable = { 			which = population_needing_education					value = 1 }
				change_variable = { 			which = university_education_multiplier 				value = 1 }
				PREV = { subtract_variable = {  which = university_available_contribution				value = 1 } }
				
				if = {
					limit = {
						owned_by = PREV
					}
					change_variable = { 			which = university_domestic_student						value = 1 }
					
					PREV = { subtract_variable = { which = university_local_contribution value = 1 } }
				}
				if = {
					limit = {
						NOT = { owned_by = PREV }
					}
					PREV = { change_variable = { 	which = university_foreign_students						value = 1 } }
					change_variable = { 			which = university_student_sent_abroad 					value = 1 }
				}
				
				# A portion of province's art power is sent to the university
				set_variable = { which = art_power_sent value = 1 }
				
				if = {
					limit = {
						check_variable = { which = upper_class_population value = 1 }
					}
					if = {
						limit = {
							is_variable_equal = {
								which = upper_class_population
								value = 0
							}
						}
						
						# log = "<ERROR><D775CEE0><THIS:[This.GetName]><PREV:[Prev.GetName]><ROOT:[Root.GetName]><FROM:[From.GetName]> Division by zero!"
					}
					else = {
						divide_variable = {
							which = art_power_sent
							which = upper_class_population
						}
					}
				}
				
				divide_variable = { which = art_power_sent value = 2 }
				multiply_variable = { which = art_power_sent which = art_power }
				
				subtract_variable = { which = art_power which = art_power_sent }
				
				PREV = { change_variable = { which = art_power_sent which = PREV } }
			}
		}
		
		if = {
			limit = {
				NOT = { has_global_flag = student_hunt_continue }
			}
			set_variable = { which = itr value = 0 }
		}
		clr_global_flag = student_hunt_continue
	}
}

university_head_hunting_small = {
	set_province_flag = university_head_hunted
	
	set_variable = { which = itr value = 1 }
	
	while = {
		limit = {
			check_variable = { which = itr value = 1 }
			check_variable = { which = university_available_contribution value = 1 }
		}
		
		set_variable = { which = art_power_compare value = 0 }
		
		every_province = {
			limit = {
				OR = {
					region = PREV
					
					culture_group = PREV
				}
				check_variable = { which = population_needing_education	value = 1 }
				religion_group = PREV
				#	NOT = { has_province_flag = university_present }
			}
			set_variable = { which = art_power_compare which = art_power }
			
			if = {
				limit = {
					PREV = { owner = { has_country_flag = sponsor_domestic_students } }
					
					owned_by = PREV
				}
				multiply_variable = { which = art_power_compare value = 4 }
			}
			
			if = {
				limit = {
					PREV = { check_variable = { which = university_local_contribution value = 1 } }
					
					owned_by = PREV
				}
				change_variable = { which = art_power_compare value = 1 }
				multiply_variable = { which = art_power_compare value = 10 }
			}
			
			if = {
				limit = {
					culture = PREV
				}
				multiply_variable = { which = art_power_compare value = 1.5 }
			}
			if = {
				limit = {
					culture_group = PREV
				}
				multiply_variable = { which = art_power_compare value = 1.25 }
			}
			
			if = {
				limit = {
					religion = PREV
				}
				multiply_variable = { which = art_power_compare value = 1.5 }
			}
			
			if = {
				limit = {
					area = PREV
				}
				multiply_variable = { which = art_power_compare value = 2 }
			}
			
			if = {
				limit = {
					check_variable = { which = art_power_compare which = PREV }
				}
				set_province_flag = foreign_university_cand
				
				PREV = { set_variable = { which = art_power_compare which = PREV } }
			}
		}
		
		every_province = {
			limit = {
				has_province_flag = foreign_university_cand
			}
			clr_province_flag = foreign_university_cand
			
			if = {
				limit = {
					is_variable_equal = { which = art_power_compare which = PREV }
					
					PREV = { check_variable = { which = university_available_contribution value = 1 } }
				}
				set_global_flag = student_hunt_continue
				
				subtract_variable = { 			which = population_needing_education					value = 1 }
				change_variable = { 			which = university_education_multiplier 				value = 1 }
				PREV = { subtract_variable = {  which = university_available_contribution				value = 1 } }
				
				if = {
					limit = {
						owned_by = PREV
					}
					change_variable = { 			which = university_domestic_student						value = 1 }
					PREV = { subtract_variable = { which = university_local_contribution value = 1 } }
				}
				if = {
					limit = {
						NOT = { owned_by = PREV }
					}
					PREV = { change_variable = { 	which = university_foreign_students						value = 1 } }
					change_variable = { 			which = university_student_sent_abroad 					value = 1 }
				}
				
				# A portion of province's art power is sent to the university
				set_variable = { which = art_power_sent value = 1 }
				
				if = {
					limit = {
						check_variable = { which = upper_class_population value = 1 }
					}
					if = {
						limit = {
							is_variable_equal = {
								which = upper_class_population
								value = 0
							}
						}
						
						# log = "<ERROR><75C7A4E0><THIS:[This.GetName]><PREV:[Prev.GetName]><ROOT:[Root.GetName]><FROM:[From.GetName]> Division by zero!"
						
					}
					else = {
						divide_variable = {
							which = art_power_sent
							which = upper_class_population
						}
					}
				}
				
				divide_variable = { which = art_power_sent value = 2 }
				multiply_variable = { which = art_power_sent which = art_power }
				
				subtract_variable = { which = art_power which = art_power_sent }
				
				PREV = { change_variable = { which = art_power_sent which = PREV } }
			}
		}
		
		if = {
			limit = {
				NOT = { has_global_flag = student_hunt_continue }
			}
			set_variable = { which = itr value = 0 }
		}
		clr_global_flag = student_hunt_continue
	}
}