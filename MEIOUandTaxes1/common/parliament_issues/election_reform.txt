election_reform = {
	
	category = 3
	
	allow = {
		government = republic
	}
	
	effect = {
		add_republican_tradition = 10
	}
	
	modifier = {
		republican_tradition = 0.5
	}
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 2
			NOT = { republican_tradition = 60 }
		}
	}
}