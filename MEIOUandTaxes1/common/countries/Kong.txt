#Country Name: Please see filename.

graphical_culture = africangfx

color = { 137  98  195 }

historical_idea_groups = {
	trade_ideas
	logistic_ideas
	leadership_ideas
	spy_ideas
	diplomatic_ideas
	economic_ideas
	administrative_ideas
	quantity_ideas
}

historical_units = { #Pastoralists and Coastal states
	sudanese_tuareg_camelry
	sudanese_archer_infantry
	sudanese_tribal_raider_light_cavalry
	sudanese_sword_infantry
	sudanese_war_raider_light_cavalry
	sudanese_sofa_infantry
	sudanese_pony_archer_cavalry
	sudanese_sofa_musketeer_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_gold_coast_infantry
	sudanese_musketeer_infantry
	sudanese_carabiner_cavalry
	sudanese_countermarch_musketeer_infantry
	sudanese_sofa_rifle_infantry
	sudanese_rifled_infantry
	sudanese_hunter_cavalry
	sudanese_impulse_infantry
	sudanese_lancer_cavalry
	sudanese_breech_infantry
}

monarch_names = {
	"Agyen Kokobo #0" = 20
	"Ofusu Kwabi #0" = 20
	"Oduro #0" = 20
	"Ado #0" = 20
	"Otumfo Asare #0" = 20
	"Yanza #0" = 20
	"Ansa Saseraku #0" = 20
	"Akotia #0" = 20
	"Abuako Dako #0" = 20
	"Afera Kuma #0" = 20
	"Manukure #0" = 20
	"Akwamno Panyini #0" = 20
	"Dako Booman #0" = 20
	"Mnumunumfi #0" = 20
	"Werempe Ampem #0" = 20
	"Boa Amponsem #0" = 20
	"Ntim Gyakari #0" = 20
	"Boado Ahafo Berempon #0" = 20
	"Kyei Akobeng #0" = 20
	"Amoako Atta Panyin #0" = 20
	"Gyan Badu #0" = 20
	"Amoako Atta Kuma #0" = 20
	"Amoako Atta Yiadom #0" = 20
	"Owusu Bori #0" = 20
	"Kwadwo Tibu #0" = 20
	"Kwakye Fram #0" = 20
	"Kwesi Kyei #0" = 20
	"Nkwantabisa #0" = 20
	
	"Adaeze #0" = -1
	"Chidi #0" = -1
	"Chiamaka #0" = -1
	"Amarachi #0" = -1
}

leader_names = {
	Addy
	Afrifa
	Sadami
	Sabah
	Zakari
	Offei
	Donkor
	Akoto
	Sribor
	Moyoyo
}

ship_names = {
	Nyame "Asase Yaa" Bia Tano
	Dwoada Benada Wukuada Yawoada
	Fiada Memeneda Kwasiada
}
