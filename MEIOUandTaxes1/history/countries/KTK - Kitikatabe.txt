# KTK - Kitikatabe clan
# 2010-jan-21 - FB - HT3 changes

government = japanese_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = kansai
religion = mahayana
technology_group = chinese
capital = 2282	# Ise

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1356.1.10 = {
	monarch = {
		name = "Akiyoshi"
		dynasty = "Kitabatake"
		birth_date = 1326.1.1
		#death_date = 1383.12.30
		ADM = 3
		DIP = 4
		MIL = 5
	}
}

1429.1.25 = {
	monarch = {
		name = "Noritomo"
		dynasty = "Kitabatake"
		birth_date = 1423.1.1
#		death_date = 1471.4.13
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1453.1.1 = {
	heir = {
		name = "Masasato"
		monarch_name = "Masasato"
		dynasty = "Kitabatake"
		birth_date = 1448.1.1
		death_date = 1508.12.4
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1471.4.13 = {
	monarch = {
		name = "Masasato"
		dynasty = "Kitabatake"
		birth_date = 1423.1.1
#		death_date = 1471.4.13
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1471.4.13 = {
	heir = {
		name = "Kichika"
		monarch_name = "Kichika"
		dynasty = "Kitabatake"
		birth_date = 1468.1.1
		death_date = 1517.1.1
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1508.12.4 = {
	monarch = {
		name = "Kichika"
		dynasty = "Kitabatake"
		birth_date = 1468.1.1
#		death_date = 1517.1.1
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1508.12.4 = {
	heir = {
		name = "Harutomo"
		monarch_name = "Harutomo"
		dynasty = "Kitabatake"
		birth_date = 1503.1.1
		death_date = 1563.10.4
		claim = 90
		ADM = 4
		DIP = 3
		MIL = 4
	}
}

1517.1.1 = {
	monarch = {
		name = "Harutomo"
		dynasty = "Kitabatake"
		birth_date = 1503.1.1
#		death_date = 1563.10.4
		ADM = 4
		DIP = 3
		MIL = 4
	}
}

1528.1.1 = {
	heir = {
		name = "Tomonori"
		monarch_name = "Tomonori"
		dynasty = "Kitabatake"
		birth_date = 1528.1.1
		death_date = 1576.12.15
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1563.10.4 = {
	monarch = {
		name = "Tomonori"
		dynasty = "Kitabatake"
		birth_date = 1528.1.1
#		death_date = 1576.12.15
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1563.10.4 = {
	heir = {
		name = "Tomofusa"
		monarch_name = "Tomofusa"
		dynasty = "Kitabatake"
		birth_date = 1547.1.1
		death_date = 1580.1.21
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1576.12.15 = {
	monarch = {
		name = "Nobukatsu"
		dynasty = "Kitabatake" # Oda Nobukatsu, a son of Oda Nobunaga
		birth_date = 1558.1.1
#		death_date = 1630.6.30
		ADM = 2
		DIP = 2
		MIL = 3
	}
}
