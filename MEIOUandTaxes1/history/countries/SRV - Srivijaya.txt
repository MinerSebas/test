# SRV - Srivijaya

government = eastern_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = sumatran
religion = vajrayana
technology_group = austranesian
capital = 3921	# Palembang

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1324.1.1 = {
	monarch = {
		name = "Sang Nila Utama"
		dynasty = "Svirijaya"
		ADM = 5
		DIP = 3
		MIL = 2
	}
}

1343.1.1 = {
	heir = {
		name = "Bikrama Vira Diraja"
		monarch_name = "Bikrama Vira Diraja"
		dynasty = "Svirijaya"
		birth_Date = 1305.1.1
		death_date = 1386.1.1
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1372.1.1 = {
	monarch = {
		name = "Bikrama Vira Diraja"
		dynasty = "Svirijaya"
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1372.1.1 = {
	heir = {
		name = "Rana Wira Kerma"
		monarch_name = "Rana Wira Kerma"
		dynasty = "Svirijaya"
		birth_Date = 1324.1.1
		death_date = 1386.1.1
		claim = 95
		ADM = 4
		DIP = 1
		MIL = 1
	}
}

1386.1.1 = {
	monarch = {
		name = "Rana Wira Kerma"
		dynasty = "Svirijaya"
		ADM = 4
		DIP = 1
		MIL = 1
	}
}

1386.1.1 = {
	heir = {
		name = "Parameswara"
		monarch_name = "Parameswara"
		dynasty = "Svirijaya"
		birth_Date = 1344.1.1
		death_date = 1414.1.1
		claim = 95
		ADM = 4
		DIP = 2
		MIL = 2
	}
}

1399.1.1 = {
	monarch = {
		name = "Parameswara"
		dynasty = "Svirijaya"
		ADM = 4
		DIP = 2
		MIL = 2
	}
}
