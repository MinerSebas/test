# HAF - Hafsid

government = despotic_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = tunisian
add_accepted_culture = kabyle
religion = sunni
technology_group = muslim
capital = 341
historical_friend = TRP
historical_neutral = FZA

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}

1318.1.1 = {
	monarch = {
		name = "Abu Bakr II"
		dynasty = "Hafsid"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1346.1.1 = {
	monarch = {
		name = "Abu Hafs Umar II"
		dynasty = "Hafsid"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1348.1.1 = {
	monarch = {
		name = "Ahmad I"
		dynasty = "Hafsid"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1349.1.1 = {
	monarch = {
		name = "Ishaq II"
		dynasty = "Hafsid"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1369.1.1 = {
	monarch = {
		name = "Abu al-Baqa Khalid"
		dynasty = "Hafsid"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1371.1.1 = {
	monarch = {
		name = "Ahmad II"
		dynasty = "Hafsid"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1394.1.1 = {
	monarch = {
		name = "Abd al-Aziz II"
		dynasty = "Hafsid"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1434.1.1 = {
	monarch = {
		name = "Muhammad III"
		dynasty = "Hafsid"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1435.9.18 = {
	monarch = {
		name = "'Uthman"
		dynasty = "Hafsid"
		ADM = 2
		DIP = 4
		MIL = 4
	}
}

1488.1.1 = {
	monarch = {
		name = "Yahy� III"
		dynasty = "Hafsid"
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1489.1.1 = {
	monarch = {
		name = "'Abd al-Mu'min"
		dynasty = "Hafsid"
		ADM = 1
		DIP = 2
		MIL = 2
	}
}

1490.10.15 = {
	monarch = {
		name = "Zakariy�' II"
		dynasty = "Hafsid"
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1494.5.17 = {
	monarch = {
		name = "Muhammad V"
		dynasty = "Hafsid"
		ADM = 4
		DIP = 1
		MIL = 2
	}
}

1526.1.1 = {
	monarch = {
		name = "Muhammad VI"
		dynasty = "Hafsid"
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1542.1.1 = {
	monarch = {
		name = "Ahmad III"
		dynasty = "Hafsid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	define_ruler_to_general = {
		fire = 3
		shock = 2
		manuever = 3
		siege = 0
	}
}

1573.1.1 = {
	monarch = {
		name = "Muhammad VII"
		dynasty = "Hafsid"
		ADM = 4
		DIP = 3
		MIL = 2
	}
}

# Part of the Ottoman Empire
