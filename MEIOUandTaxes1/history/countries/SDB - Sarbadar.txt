# SDB - Sardabar

government = despotic_monarchy
government_rank = 1 #AMIRATE?
mercantilism = 0.0
technology_group = muslim
primary_culture = persian
religion = shiite
capital = 433 # Sabzevar
historical_rival = ATB
historical_rival = JQB

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	set_country_flag = title_2
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1338.1.1 = {
	monarch = {
		name = "Wajih ad-Din" #Killed in battle
		ADM = 3
		DIP = 3
		MIL = 3
		dynasty = "Mas'ud"
	}
	set_country_flag = sufi_influence
	add_country_modifier = { name = obstacle_succession duration = -1 }
}
1344.1.1 = {
	monarch = {
		name = "Muhammad" #Military commander who took over after Mas'ud died.  Overthrown after 3 years
		ADM = 2
		DIP = 2
		MIL = 3
		dynasty = "Aytimur"
	}
	add_legitimacy = -20
}
1346.9.1 = {
	monarch = {
		name = "Kulu" #Another of Mas'ud's military commanders, installed by the pro-Dervish party.
		ADM = 2
		DIP = 2
		MIL = 3
		dynasty = "Isfandayar"
	}
	set_country_flag = sufi_patronage
}
1347.1.1 = {
	heir = {
		name = "Luft Allah" #Son of Mas'ud
		monarch_name = "Luft Allah"
		ADM = 2
		DIP = 2
		MIL = 2
		dynasty = "Mas'ud"
		birth_date = 1337.1.1
		death_date = 1361.1.1
		claim = 90
	}
}
1347.1.1 = {
	monarch = {
		name = "Shamsuddin ibn Fazl Allah" #Brother of Mas'ud, forced to abdicate
		ADM = 2
		DIP = 2
		MIL = 2
		dynasty = "Mas'ud"
	}
	add_legitimacy = -10
}
1348.1.1 = {
	monarch = {
		name = "Khwaja Shams al-Din" #Aristocrat and guild leader; Murdered; pro-Dervish; suppressed corruption; fanatical Shi'ite; established moral police
		ADM = 3
		DIP = 1
		MIL = 3
		dynasty = "'Ali"
	}
	religion = sunni
	set_country_flag = was_shiite
	change_variable = { which = sympathy_to_old_religion value = 100 }
	add_legitimacy = -10
	#Official religion of the state Sunni to avoid alienating moderates
}
1351.1.1 = {
	monarch = {
		name = "Yahya" #An aristocrat, murdered; a religious moderate; protected the murderer of Shamsuddin; tricked and murdered Togha Temur
		ADM = 4
		DIP = 2
		MIL = 4
		dynasty = "Karawi"
	}
	clr_country_flag = sufi_patronage #Dismantled many social programs
}
1355.1.1 = {
	monarch = {
		name = "Zahir al-Din" #Nephew of Yahya, installed and then deposed by Haydar
		ADM = 1
		DIP = 1
		MIL = 1
		dynasty = "Karawi"
	}
	add_legitimacy = 10
}
1356.1.1 = {
	monarch = {
		name = "Haydar" #Murderer of Shamsuddin; deposed Zahir; stabbed to death by a slave on campaign
		ADM = 2
		DIP = 2
		MIL = 2
		dynasty = "Qassab"
	}
	add_legitimacy = -10
}
1356.10.1 = {
	monarch = {
		name = "Lutf Allah" #Son of Mas'ud, finally becomes ruler thanks to a revolt in his name; mostly a figurehead ruler; deposed and executed by Hasan
		ADM = 2
		DIP = 2
		MIL = 2
		dynasty = "Mas'ud"
	}
	add_legitimacy = 20
}
1358.1.1 = {
	monarch = {
		name = "Hasan" #The power behind the throne; ruled directly from 1361; failed to overcome rebellion in Astarabad over 3 campaigns; was executed by his own side to please Ali-yi Mu'ayyad who had conquered the capital
		ADM = 3
		DIP = 2
		MIL = 2
		dynasty = "Damghani"
	}
	add_legitimacy = -20
}
1362.1.1   = {
	monarch = {
		name = "'Ali-yi" #Forced to seek Timur's help against a Dervish rebellion
		ADM = 3
		DIP = 4
		MIL = 3
		dynasty = "Mu'ayyad"
	}
	add_legitimacy = 20
	clr_country_flag = was_shiite
	set_country_flag = was_sunni
	religion = shiite #Agreed to convert as part of deal with Dervishes who fought for him
	clr_country_flag = sufi_influence #Turned against the Dervishes soon after
}
1376.1.1 = { #The Dervishes threw off the Sarbardars with Shah Shuja's support and established their own government
	government = theocratic_government
	monarch = {
		name = "Rukn al-Din" #Dervish leader
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
1377.1.1 = {
	government = despotic_monarchy
	monarch = {
		name = "'Ali-yi" #Returns to power
		ADM = 3
		DIP = 4
		MIL = 3
		dynasty = "Mu'ayyad"
	}
}

# 1381 - Subjected to Tamerlane
