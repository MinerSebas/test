# ROM - Rome

government = papal_government government_rank = 1
mercantilism = 0.0
primary_culture = umbrian
religion = catholic
technology_group = western
capital = 2530
fixed_capital = 2530 # Roma

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	add_absolutism = -100
	add_absolutism = 10
}
