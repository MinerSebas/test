# YUK - Y�ki
# LS/GG - Japanese Civil War
# 2010-jan-20 - FB - HT3
# 2013-aug-07 - GG - EUIV changes

government = japanese_monarchy
government_rank = 1
# aristocracy_plutocracy = -4
# centralization_decentralization = 2
# innovative_narrowminded = 2
mercantilism = 0.0 # mercantilism_freetrade = -5
# offensive_defensive = -1
# land_naval = 0
# quality_quantity = 4
# serfdom_freesubjects = -5
# isolationist_expansionist = -3
# secularism_theocracy = 0
primary_culture = kanto
religion = mahayana
technology_group = chinese
capital = 3344	# Shimosa

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1330.1.1 = {
	monarch = {
		name = "Naomitsu"
		dynasty = "Y�ki"
		ADM = 2
		DIP = 2
		MIL = 5
	}
}

1349.1.1 = {
	heir = {
		name = "Motomitsu"
		monarch_name = "Motomitsu"
		dynasty = "Y�ki"
		birth_date = 1349.1.1
		death_date = 1430.1.1
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 4
	}
}
