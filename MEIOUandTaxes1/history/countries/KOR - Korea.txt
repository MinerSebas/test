# KOR - Korea
# MEIOU-GG Religion changes
# 2010-jan-21 - FB - HT3 changes
# 2010-dec-31 - FB - ASSA, changes recommended by assalala (use personal names)
# http://en.wikipedia.org/wiki/List_of_Korean_monarchs

government = chinese_monarchy_2 government_rank = 1
# innovative_narrowminded = -2
mercantilism = 0.0
primary_culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392
technology_group = chinese
capital = 2802	# Songdo

historical_rival = YUA
historical_rival = MCH
historical_rival = JOS
historical_rival = TMN


918.7.25 = {
	monarch = {	#Taejo
		name = "Geon"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
	#Instability
	add_stability = -2
	add_corruption = 30
	add_legitimacy = -60
	
	#Obstacle
	add_country_modifier = {
		name = obstacle_shifting_loyalties
		duration = -1
	}
	add_country_modifier = {
		name = obstacle_succession
		duration = -1
	}
	add_country_modifier = {
		name = obstacle_military_administration
		duration = -1
	}
}
943.7.4 = {
	monarch = {	#Hyejong
		name = "Mu"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
945.1.1 = {
	monarch = {	#Jeongjong
		name = "Yo"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
949.4.13 = {
	monarch = {	#Gwangjong
		name = "So"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
975.7.4 = {
	monarch = { #Gyeongjong
		name = "Ju"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
981.8.13 = {
	monarch = { #Seongjong
		name = "Chi"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
997.11.29 = {
	monarch = { #Mokjong
		name = "Song"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}
1009.3.2 = {
	monarch = {	#Hyeonjong
		name = "Sun"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1031.6.17 = {
	monarch = {	#Deokjong
		name = "Heum"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1034.10.13 = {
	monarch = { #Jeongjong
		name = "Hyeong"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1046.6.24 = {
	monarch = {	#Munjong
		name = "Hwi"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1083.9.2 = {
	monarch = {	#Sunjong
		name = "Hun"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1083.12.5 = {
	monarch = { #Seonjong
		name = "Un"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1094.6.17 = {
	monarch = { #Heonjong
		name = "Uk"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1095.11.6 = {
	monarch = { #Sukjong
		name = "Hui"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1105.11.10 = {
	monarch = { #Yejong
		name = "U"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1122.5.15 = {
	monarch = {	#Injong
		name = "Hae"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1146.4.10 = {
	monarch = {	#Uijong
		name = "Hyeon"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1170.10.7 = {
	monarch = {	#Myeongjong
		name = "Ho"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1197.1.1 = {
	monarch = {	#Sinjong
		name = "Tak"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1204.2.15 = {
	monarch = { #Huijong
		name = "Yeong"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1211.2.15 = {
	monarch = { #Gangjong
		name = "O"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1212.8.26 = {
	monarch = { #Gojong
		name = "Cheol"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1259.7.21 = {
	monarch = {	#Wonjong
		name = "Jeong"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1274.7.23 = {
	monarch = {	#Chungnyeol
		name = "Geo"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1308.7.30 = {
	monarch = { #Chungseok
		name = "Jang"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1313.7.30 = {
	monarch = { #Chungsuk
		name = "Man"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1339.5.3 = {
	monarch = {	#Chunghye
		name = "Jeong"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1344.1.30 = {
	monarch = { #Chungmok
		name = "Heun"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1348.12.25 = {
	monarch = { #Chungjeong
		name = "Jeo"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
	}
}
1351.5.23 = {
	monarch = { #Gongmin
		name = "Jeon"
		dynasty = "Wang"
		ADM = 4
		DIP = 5
		MIL = 6
		birth_date = 1330.5.23
	}
	queen = {
		country_of_origin = YUA
		name = "Budashiri"
		dynasty = "Kublaid"
		culture = mongol
		birth_date = 1333.1.1
		death_date = 1365.3.8
		female = yes
		ADM = 4
		DIP = 4
		MIL = 2
	}
}

1356.1.1 = {
	define_general = {
		name = "Yeong Choi"
		fire = 3
		shock = 3
		manuever = 3
		siege = 2
		#death_date = 1388.7.18
	}
}

1365.1.1 = {
	heir = {
		name = "U"
		monarch_name = "U"
		dynasty = "Wang"
		birth_date = 1365.1.1
		death_date = 1389.1.1
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 2
	}
}

1374.10.27 = {
	monarch = {
		name = "U"
		dynasty = "Wang"
		ADM = 1
		DIP = 1
		MIL = 2
	}
}
1379.4.1 = {
	queen = {
		country_of_origin = KOR
		name = "Geunbi"
		dynasty = "Yi"
		birth_date = 1353.1.1
		death_date = 1410.1.1
		female = yes
		ADM = 4
		DIP = 4
		MIL = 2
	}
}

1380.3.21 = {
	heir = {
		name = "Chang"
		monarch_name = "Chang"
		dynasty = "Wang"
		birth_date = 1380.2.11
		death_date = 1389.12.31
		claim = 95
		ADM = 1
		DIP = 1
		MIL = 1
	}
}	#FB-ASSA

1388.7.18 = {
	monarch = {
		name = "Chang"
		dynasty = "Wang"
		ADM = 1
		DIP = 1
		MIL = 1
	}
	define_general = {
		name = "Seong-gye Yi"
		fire = 5
		shock = 4
		manuever = 5
		siege = 3
		#death_date = 1392.1.1
	}
}

1389.4.17 = {
	monarch = {
		name = "Yo"
		dynasty = "Wang"
		ADM = 3
		DIP = 2
		MIL = 1
	}
	queen = {
		country_of_origin = KOR
		name = "Sunbi"
		dynasty = "No"
		birth_date = 1372.1.1
		death_date = 1394.1.1
		female = yes
		ADM = 4
		DIP = 4
		MIL = 2
	}
}	#FB-ASSA
