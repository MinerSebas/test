# KAM - Kingdom of Armenia
# aka Armenian Kingdom of Cilicia
# 2010-jan-16 - FB - HT3 changes

government = feudal_monarchy government_rank = 1
mercantilism = 10
primary_culture = cilician
religion = coptic#DEI GRATIA
technology_group = eastern
capital = 1755	# Cilicia

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}

1320.7.20 = {
	monarch = {
		name = "Leon IV"
		dynasty = "Hethumid"
		ADM = 3
		DIP = 4
		MIL = 3
	}
	set_country_flag = native_greek_state
}

1341.8.28 = {
	monarch = {
		name = "Gosdantin II"
		dynasty = "de Lusignan"
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1344.4.17 = {
	monarch = {
		name = "Gosdantin III"
		dynasty = "de Lusignan"
		ADM = 5
		DIP = 4
		MIL = 2
	}
	religion = catholic
	set_country_flag = was_coptic
	set_variable = { which = "sympathy_to_old_religion" value = 60 }
}

1362.12.21 = {
	monarch = {
		name = "Gosdantin IV"
		dynasty = "de Lusignan"
		ADM = 3
		DIP = 4
		MIL = 2
	}
}

1374.1.1 = {
	monarch = {
		name = "Leon V"
		dynasty = "de Lusignan"
		ADM = 5
		DIP = 4
		MIL = 1
	}
} # last king of Armenia
