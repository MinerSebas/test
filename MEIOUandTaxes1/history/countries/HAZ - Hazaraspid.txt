# HAZ - Hazaraspid

government = despotic_monarchy government_rank = 1
mercantilism = 0.0
technology_group = muslim
primary_culture = kurdish
religion = sunni
capital = 413 # Khorramabab

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1355.1.1   = {
	monarch = {
		name = "Shams al din Pashang"
		dynasty = "Hazaraspid"
		ADM = 5
		DIP = 4
		MIL = 5
	}
}

1378.1.1   = {
	monarch = {
		name = "Malek Pir Ahmad"
		dynasty = "Hazaraspid"
		ADM = 5
		DIP = 4
		MIL = 5
	}
}

1408.1.1   = {
	monarch = {
		name = "Abu Saeed"
		dynasty = "Hazaraspid"
		ADM = 5
		DIP = 4
		MIL = 5
	}
}

1417.1.1   = {
	monarch = {
		name = "Shah Hussein"
		dynasty = "Hazaraspid"
		ADM = 5
		DIP = 4
		MIL = 5
	}
}

1424.1.1   = {
	monarch = {
		name = "Ghias al din"
		dynasty = "Hazaraspid"
		ADM = 5
		DIP = 4
		MIL = 5
	}
}

# 1383 - Subjected to Tamerlane
