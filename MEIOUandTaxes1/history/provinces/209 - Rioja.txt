# 209 - La Rioja

owner = CAS #Juan II de Castilla
controller = CAS

capital = "Logro�o"
trade_goods = wine
culture = castillian
religion = catholic

hre = no

base_tax = 8
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
road_network = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1 = {
	add_core = CAS
	add_core = NAV
	add_core = ENR
}
1360.1.1 = {
	controller = ENR
}
1369.3.23 = {
	controller = CAS
	remove_core = ENR
}
1500.3.3 = {
	base_tax = 9
	base_production = 1
	base_manpower = 1
}
1512.7.24 = {
	remove_core = NAV
} # End of the Kingdom of Navarra.
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no
	paved_road_network = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1713.4.11 = {
	remove_core = CAS
}
1813.6.21 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
