# No previous file for Saponi

capital = "Saponi"
trade_goods = unknown
culture = shawnee
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 1
native_hostileness = 6

450.1.1 = {
	set_province_flag = tribals_control_province
}
1650.1.1 = {
	owner = SSQ
	controller = SSQ
	add_core = SSQ
	is_city = yes
	trade_goods = fur
} # Full extent of the Susquehannock at start of the Beaver Wars
1671.1.1 = { } # Abraham Wood
1672.1.1 = {
	owner = IRO
	controller = IRO
	culture = iroquois
	citysize = 100
} # Iroquois drives off other natives in Beaver Wars
1768.11.5 = {
	owner = GBR
	controller = GBR
	culture = american
	religion = protestant
} # Treaty of Fort Stanwix, becomes part of Virginia
1774.1.1 = {
	citysize = 1000
	capital = "Fort Blair"
	unrest = 6
}
1776.7.4 = {
	owner = USA
	controller = USA
	culture = american
} # Declaration of independence
1782.11.1 = {
	unrest = 0
} # Preliminary articles of peace, the British recognized American independence
1801.7.4 = {
	add_core = USA
}
