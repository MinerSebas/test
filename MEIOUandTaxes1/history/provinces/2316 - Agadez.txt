# 2316 - Agadez

owner = AIR
controller = AIR
add_core = AIR

capital = "Agadez"
trade_goods = palm_date
culture = tuareg
religion = sunni

hre = no

base_tax = 2
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
marketplace = yes
local_fortification_1 = yes

discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 64 }
	set_province_flag = mined_goods
	set_province_flag = copper
}
1515.1.1 = {
	temple = yes
	base_production = 3
}