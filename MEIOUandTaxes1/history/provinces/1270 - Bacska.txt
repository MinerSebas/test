# 1270 - B�cska

owner = HUN
controller = HUN
add_core = HUN

capital = "Zombor"
trade_goods = wheat
culture = hungarian
religion = catholic

hre = no

base_tax = 9
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1506.1.1 = {
	controller = REB
} # Szekely rebellion
1507.1.1 = {
	controller = HUN
} # Estimated
1514.4.1 = {
	controller = REB
} # Peasant rebellion against Hungary's magnates
1515.1.1 = {
	controller = HUN
} # Estimated
1520.5.5 = {
	base_tax = 11
	base_production = 0
	base_manpower = 1
}
1526.8.30 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_permanent_claim = HAB
} # The Ottomans defeat the Hungarians at the Battle of Moh�cs.
1541.1.1 = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1699.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
} # The Ottomans surrender to the army led by Louis of Baden
