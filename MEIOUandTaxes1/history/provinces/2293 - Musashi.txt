# 2293 - Musachi
# GG/LS - Japanese Civil War

owner = USG
controller = USG
add_core = USG

capital = "Yedo"
trade_goods = fish
culture = kanto
religion = mahayana #shinbutsu

hre = no

base_tax = 41
base_production = 2
base_manpower = 3

is_city = yes
workshop = yes
town_hall = yes
marketplace = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes
#Engaku-ji

discovered_by = chinese

#One of the Six Old Kilns
450.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = awesome_natural_place
	add_permanent_province_modifier = {
		name = "musashi_large_natural_harbor"
		duration = -1
	}
}
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_chinaware
		duration = -1
	}
}
1356.1.1 = {
	add_core = HJO
} # Owned by a vassal of the Uesugi clan
1493.1.1 = {
	owner = HJO
	controller = HJO
} # Hojo Soun gains control of Izu Province
1501.1.1 = {
	base_tax = 71
	base_production = 5
	base_manpower = 6
	art_corporation = yes
}
1542.1.1 = {
	discovered_by = POR
}
1590.8.4 = {
	owner = ODA
	controller = ODA
} # Toyotomi Hideyoshi takes Odawara Castle, Hojo Ujimasa commits seppuku
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
