# 1058 - Barguzinski

owner = MYR
controller = MYR
add_core = MYR

capital = "Yaksa"
trade_goods = fur
culture = daur
religion = tengri_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1643.1.1 = {
	discovered_by = RUS
} # Vasily Poyarkov
1650.9.1 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	capital = "Albazin"
}
1652.1.1 = {
	owner = QNG
	controller = QNG
}
1655.1.1 = {
	owner = MYR
	controller = MYR
}
1675.1.1 = {
	owner = RUS
	controller = RUS
}
1689.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = RUS
}
1709.1.1 = {
	discovered_by = SPA
}
