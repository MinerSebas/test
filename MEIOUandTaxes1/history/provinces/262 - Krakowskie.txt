# 262 -Malopolskiye
# Krakow

owner = POL
controller = POL
add_core = POL

capital = "Krak�w"
trade_goods = wheat
culture = polish
religion = catholic

hre = no

base_tax = 3
base_production = 1
base_manpower = 0

is_city = yes
merchant_guild = yes
road_network = yes
temple = yes
urban_infrastructure_2 = yes
fort_14th = yes

discovered_by = western
discovered_by = eastern
discovered_by = turkishtech
discovered_by = steppestech

450.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = salt
	add_permanent_province_modifier = {
		name = wieliczka
		duration = -1
	}
}
1100.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_cloth
		duration = -1
	}
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1364.5.12 = {
	small_university = yes
}
1508.1.1 = {
	fort_14th = no
	fort_16th = yes
}
1520.5.5 = {
	base_tax = 2
	base_production = 3
	base_manpower = 1
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
# 1519-1533, Wawel cathedral
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1587.1.1 = {
	unrest = 6
}
1587.2.14 = {
	unrest = 0
} # After a 6 week siege, Maximillian drops his bid for the Polish trone
1588.1.1 = {
	controller = REB
} # Civil war, Polish succession
1589.1.1 = {
	controller = PLC
} # Coronation of Sigismund III
1596.1.1 = { } #Polish capital moved to Warsawa
1733.1.1 = {
	controller = REB
} # The war of Polish succession
1735.1.1 = {
	controller = PLC
}
1768.2.28 = {
	unrest = 8
} # Became a center of the first Polish uprisings against the Polish king & Russia
1794.3.24 = {
	unrest = 5
} # Kosciuszko uprising
1794.11.16 = {
	unrest = 0
} # The end of the uprising
1795.10.24 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = POL
	remove_core = PLC
	remove_core = RUS
} # Third partition
1809.10.14 = {
	owner = POL
	controller = POL
	remove_core = HAB
} # The Duchy of Warsaw
1815.6.9 = {
	owner = KRA
	controller = KRA
	add_core = KRA
} # The Free city of Krakow
1846.11.16 = {
	owner = HAB
	controller = HAB
}
