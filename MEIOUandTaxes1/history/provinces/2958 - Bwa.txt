# 2958 - Bwa

owner = BKE
controller = BKE
add_core = BKE

capital = "Sia"
trade_goods = millet
culture = senufo
religion = west_african_pagan_reformed

hre = no

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 83 }
}
1520.1.1 = {
	base_tax = 8
}