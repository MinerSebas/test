# 1541 - Bambougou

owner = MAL
controller = MAL
add_core = MAL

capital = "Segu"
trade_goods = cotton
culture = bambara
religion = sunni

hre = no

base_tax = 5
base_production = 3
base_manpower = 1

is_city = yes
urban_infrastructure_1 = yes
marketplace = yes
temple = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 16 }
}
1356.1.1 = {
	add_core = SEG
}