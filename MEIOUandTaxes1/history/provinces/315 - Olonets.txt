# 315 - Olonets

owner = NOV
controller = NOV
add_core = NOV

capital = "Aunus"
trade_goods = lumber
culture = karelian
religion = orthodox

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western

500.1.1 = {
	add_permanent_province_modifier = {
		name = "fur_medium"
		duration = -1
	}
}
1478.1.14 = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = NOV
}
1521.1.1 = {
	base_tax = 4
}
1547.1.1 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1649.1.1 = {
	fort_14th = yes
}
