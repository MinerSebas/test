# 1018 - Hizen
# GG/LS - Japanese Civil War

owner = SHN
controller = SHN
add_core = SHN

capital = "Saga"
trade_goods = fish
culture = kyushu
religion = mahayana #shinbutsu

hre = no

base_tax = 32
base_production = 2
base_manpower = 2

is_city = yes
marketplace = yes
town_hall = yes
harbour_infrastructure_2 = yes

discovered_by = chinese

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "chikuzen_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_core = RZJ
}
1501.1.1 = {
	base_tax = 54
	base_production = 5
	base_manpower = 4
}
1542.1.1 = {
	discovered_by = POR
}
1553.1.1 = {
	owner = RZJ
	controller = RZJ
	add_core = RZJ
}
1585.1.1 = {
	owner = SHN
	controller = SHN
	religion = catholic
}
1590.1.1 = {
	owner = SMZ
	controller = SMZ
	add_core = SMZ
}
1615.1.1 = {
	religion = mahayana #shinbutsu
}
1650.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
