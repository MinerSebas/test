# 1242 - Fiji

capital = "Fiji"
trade_goods = unknown # fish
culture = melanesian
religion = polynesian_religion

hre = no

base_tax = 7
base_production = 0
base_manpower = 0

native_size = 65
native_ferocity = 0.5
native_hostileness = 2

450.1.1 = {
	set_province_flag = tribals_control_province
}
1643.1.1 = {
	discovered_by = NED
} # Abel Tasman
