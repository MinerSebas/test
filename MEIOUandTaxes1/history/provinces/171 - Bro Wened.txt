# 171 Bro Gwened - Principal cities: Lorient, Vannes
# MEIOU-GG - Hundred Year War

owner = BRI
controller = BRI
add_core = BRI

capital = "Gwened"
trade_goods = wheat
culture = breton
religion = catholic

hre = no

base_tax = 18
base_production = 1
base_manpower = 1

is_city = yes
temple = yes
harbour_infrastructure_1 = yes
workshop = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_naval_supplies
		duration = -1
	}
}
1300.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "morbihan_natural_harbour"
		duration = -1
	}
}
1341.4.30 = {
	owner = MNF
	controller = MNF
	add_core = MNF
	add_core = BLO
	remove_core = BRI
} # Jean III de Bretagne dies in Caen
1365.4.12 = {
	owner = BRI
	controller = BRI
	add_core = BRI
	remove_core = BLO
	remove_core = MNF
} # End of the Brittany war of succession with the death of Charles de Blois
1378.1.1 = {
	add_core = FRA
} # Charles V invades Brittany without resistance
1440.1.1 = {
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 21
	base_production = 2
	base_manpower = 1
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1530.8.4 = {
	owner = FRA
	controller = FRA
} # Union Treaty
1550.1.1 = {
	fort_14th = yes
}
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = {
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
1650.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1740.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1793.3.7 = { } # Guerres de l'Ouest
1796.12.23 = { } # The last rebels are defeated at the battle of Savenay
1799.10.15 = { } # Guerres de l'Ouest
