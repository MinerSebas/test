# 1239 - Vanuatu

capital = "Vanuatu"
trade_goods = unknown
culture = melanesian
religion = polynesian_religion

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

native_size = 15
native_ferocity = 2
native_hostileness = 9

450.1.1 = {
	set_province_flag = tribals_control_province
}
1606.1.1 = {
	discovered_by = SPA
} # Luis V�ez de Torres and Pedro Fern�ndez de Quir�s
1770.1.1 = {
	discovered_by = GBR
	owner = GBR
	controller = GBR
	culture = english
	religion = protestant
	citysize = 100
	capital = "New Hebrides"
	trade_goods = fish
	set_province_flag = trade_good_set
} # James Cook expedition
