# 2286 - Hida
# GG/LS - Japanese Civil War

owner = KYO
controller = KYO
add_core = KYO

capital = "Takayama"
trade_goods = lumber
culture = chubu
religion = mahayana #shinbutsu

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = chinese

1477.1.1 = {
	owner = STO
	controller = STO
}
1501.1.1 = {
	base_tax = 6
}
1568.1.1 = {
	owner = ODA
	controller = ODA
}
1598.1.1 = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
