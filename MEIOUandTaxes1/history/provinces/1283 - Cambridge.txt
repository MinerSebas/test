# 1283 - Cambridge

owner = ENG
controller = ENG
add_core = ENG

capital = "Cambridge"
trade_goods = wheat
culture = english
religion = catholic

hre = no

base_tax = 9
base_production = 0
base_manpower = 1

is_city = yes
temple = yes # Salisbury is a cathedral city
local_fortification_1 = yes
road_network = yes
small_university = yes #Cambridge University

discovered_by = western
discovered_by = muslim
discovered_by = eastern

1453.1.1 = {
	unrest = 5
} # Start of the War of the Roses
1459.1.1 = {
	unrest = 7
} # Increasing Disorder & Piracy Along South Coast
1460.1.1 = {
	controller = REB
}
1461.6.1 = {
	unrest = 2
	controller = ENG
} # Coronation of Edward IV
1467.1.1 = {
	unrest = 5
} # Rivalry between Edward IV & Warwick
1471.1.1 = {
	unrest = 8
} # Unpopularity of Warwick & War with Burgundy
1471.5.4 = {
	unrest = 2
} # Murder of Henry VI & Restoration of Edward IV
1483.6.26 = {
	unrest = 8
} # Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = {
	unrest = 0
} # Battle of Bosworth Field & the End of the War of the Roses
1520.5.5 = {
	base_tax = 14
	base_production = 0
	base_manpower = 1
}
1525.1.1 = {
	fort_14th = yes
}
1529.2.5 = {
	road_network = no
	paved_road_network = yes
}
1538.1.1 = {
	religion = protestant #anglican
}
1580.1.1 = {
	religion = reformed
}
1642.8.22 = {
	controller = REB
} # Start of First English Civil War
1646.5.5 = {
	controller = ENG
} # End of First English Civil War
1648.5.11 = {
	controller = REB
}
1648.6.1 = {
	controller = ENG
}
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
