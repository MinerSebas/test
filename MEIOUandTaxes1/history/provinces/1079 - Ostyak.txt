# 1079 - Ostyak

owner = BLU
controller = BLU
add_core = BLU

capital = "Chingi-Tura"
trade_goods = fur
culture = tartar
religion = tengri_pagan_reformed

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	add_core = SIB
}
1382.1.1 = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = BLU
}
1428.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
	remove_core = GOL
}
1468.1.1 = {
	owner = SIB
	controller = SIB
	discovered_by = SIB
	add_core = SIB
	culture = siberian
	remove_core = SHY
} # Sibir Khanate formed from northern Uzbek lands
1570.1.1 = {
	religion = sunni
}
1585.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = SIB
#	culture = russian
#	religion = orthodox
	capital = "Tyumen"
} # Yermak Timofeevich, annexed to Muscovy
