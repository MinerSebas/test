# 1362 - Coblenz

owner = TRI
controller = TRI
add_core = TRI

capital = "Cowelenz"
trade_goods = wine
culture = ripuarianfranconian
religion = catholic

hre = yes

base_tax = 11
base_production = 1
base_manpower = 1

is_city = yes
temple = yes
town_hall = yes
local_fortification_1 = yes
marketplace = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1520.5.5 = {
	base_tax = 13
	fort_14th = yes
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1560.1.1 = {
	fort_14th = no
	fort_15th = yes
} # University of Trier is now handled by the Jesuites who bring a higher quality in education
# The whole country of Trier receives one law.
1690.1.1 = { } # Trier is repeatedly victim of French aggression and population declines.
1792.10.4 = {
	controller = FRA
} # Occupied by French troops
1797.10.17 = {
	owner = FRA
	add_core = FRA
} # The Treaty of Campo Formio
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1814.1.1 = {
	controller = RUS
}
1814.4.6 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = FRA
} # Napoleon abdicates
