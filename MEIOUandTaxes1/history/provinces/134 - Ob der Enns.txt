# 134 - Oberostarichi

owner = HAB
controller = HAB
add_core = HAB

capital = "Linz"
trade_goods = wheat
culture = austrian
religion = catholic

hre = yes

base_tax = 14
base_production = 1
base_manpower = 1

is_city = yes
local_fortification_1 = yes
town_hall = yes
workshop = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 31
	base_production = 1
	base_manpower = 1
	fort_14th = yes
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
