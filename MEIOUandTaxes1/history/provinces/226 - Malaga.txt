# 226 - M�laga

owner = GRA # Mustapha Sa'd King of Granada
controller = GRA
add_core = GRA

capital = "Malaga"
trade_goods = sugar
culture = andalucian # culture = eastern_andalucian
religion = sunni

hre = no

base_tax = 8
base_production = 4
base_manpower = 1

is_city = yes
urban_infrastructure_2 = yes
marketplace = yes
workshop = yes
road_network = yes
harbour_infrastructure_2 = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

450.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_naval_supplies
		duration = -1
	}
}
1356.1.1 = {
	set_province_flag = granada_emirate
	set_province_flag = arabic_name
}
1462.1.1 = {
	owner = CAS
	controller = CAS
	add_core = CAS
	rename_capital = "M�laga"
	change_province_name = "M�laga"
	trade_goods = wine
	remove_core = GRA
} # Conquest of Gibraltar by King Enrique of Castilla
1499.12.1 = {
	unrest = 2
} # The Inquisition forces Spanish muslims to convert back to Christianity. Occasional revolts occur.
1500.3.3 = {
	base_tax = 11
	base_production = 3
	base_manpower = 1
}
1502.2.1 = {
	unrest = 0
	religion = catholic
} # New capitulations where all the subjects of Granada are baptised and fully incorporated into the legal system of Castilla
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
	road_network = no
	paved_road_network = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1610.1.12 = { } # Decree for the expulsion of the morisques in Andaluc�a, which is speedily and uneventfully performed
