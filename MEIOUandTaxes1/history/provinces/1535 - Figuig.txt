# 1535 - Figuig

owner = TFL
controller = TFL
add_core = TFL

capital = "Figuig"
trade_goods = palm_date
culture = fassi
religion = sunni

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = muslim
discovered_by = turkishtech
discovered_by = soudantech
discovered_by = sub_saharan
# NOT = { discovered_by = SLL }
# NOT = { discovered_by = DSL }

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	owner = TLE
	controller = TLE
	add_core = TLE
}
1530.1.1 = {
	owner = TFL
	controller = TFL
	add_core = MOR
	remove_core = TLE
}
1549.1.1 = {
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
}
1603.1.1 = {
	unrest = 5
} # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = {
	unrest = 0
}
1631.1.1 = {
	owner = TFL
	controller = TFL
}
1668.8.2 = {
	owner = MOR
	controller = MOR
}
