# No previous file for Mamaceqtaw

capital = "Mamaceqtaw"
trade_goods = unknown
culture = ojibwa
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 2
native_hostileness = 5

450.1.1 = {
	set_province_flag = tribals_control_province
}
1650.1.1 = {
	owner = FOX
	controller = FOX
	is_city = yes
	trade_goods = fur
	add_core = FOX
}
1813.10.5 = {
	owner = USA
	controller = USA
	culture = american
	religion = protestant
} # The death of Tecumseh mark the end of organized native resistance
