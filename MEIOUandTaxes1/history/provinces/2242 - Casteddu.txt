# 2242 - Casteddu (former Giudicato of Cagliari)

owner = PIS
controller = PIS

capital = "Cast�ddu"
trade_goods = fish
culture = sardinian
religion = catholic

hre = no #AdL: was not member of the HRE

base_tax = 7
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
harbour_infrastructure_2 = yes
town_hall = yes
temple = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_great_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "sardinia_large_natural_harbour"
		duration = -1
	}
}
1088.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = lead
}
1326.1.1 = {
	owner = ARA
	controller = ARA
	add_core = ARA
	add_claim = PIS
	add_core = SAR
}
1410.3.1 = {
	remove_core = PIS
}
1516.1.23 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	remove_core = ARA
	road_network = no
	paved_road_network = yes
} # Unification of Spain
1520.5.5 = {
	base_tax = 10
	base_production = 1
	base_manpower = 0
}
1522.3.20 = {
	naval_arsenal = yes
}
1530.1.1 = {
	owner = SAR
	controller = SAR
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
}
1713.4.12 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = SPA
}
1718.8.2 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = SAR
	remove_core = HAB
} # House of Savoy becomes Kings of Sardinia
1796.1.1 = {
	controller = FRA
} # French invasion
1796.4.16 = {
	controller = SPI
} # Peace between Sardinia and France
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	add_core = SAR
}
