# 3819 - Wismar

owner = MKL
controller = MKL
add_core = MKL

capital = "Schwerin"
trade_goods = wheat
culture = pommeranian
religion = catholic

hre = yes

base_tax = 1
base_production = 2
base_manpower = 0

is_city = yes
local_fortification_1 = yes
marketplace = yes
urban_infrastructure_1 = yes
workshop = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1530.1.1 = {
	religion = protestant
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
