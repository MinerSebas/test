# 2436 - Kom�rom

owner = HUN
controller = HUN
add_core = HUN

capital = "Kom�rom"
trade_goods = lumber
culture = hungarian
religion = catholic

hre = no

base_tax = 7
base_production = 0
base_manpower = 0

is_city = yes
road_network = yes
urban_infrastructure_1 = yes
marketplace = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 8
	base_production = 0
	base_manpower = 0
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	fort_14th = no
	fort_15th = yes
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1685.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
