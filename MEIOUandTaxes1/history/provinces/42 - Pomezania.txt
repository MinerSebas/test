# 42 - Mazuri

owner = TEU
controller = TEU
add_core = TEU

capital = "Osterode"
trade_goods = lumber
culture = prussian
religion = catholic

hre = no

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
road_network = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = steppestech

1453.1.1 = {
	add_core = POL
}
1454.3.6 = {
	controller = REB
} # Beginning of the "thirteen years war"
1466.10.19 = {
	controller = TEU
} # "Peace of Torun", became a Polish fief
1520.5.5 = {
	base_tax = 9
}
1525.4.10 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = TEU
	religion = protestant
	remove_core = POL
} # Albert of Prussia became a protestant
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1560.1.1 = { } # Important port for the southeastern Baltic region
1618.8.28 = {
	owner = BRA
	controller = BRA
	add_core = BRA
} # Prussia in a personal union with Brandenburg
1701.1.18 = {
	owner = PRU
	controller = PRU
	remove_core = BRA
} # Kingdom of Prussia
