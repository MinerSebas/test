# 3735 - Henneberg

owner = HEN
controller = HEN
add_core = HEN

capital = "Henneberg"
trade_goods = wheat
culture = eastfranconian
religion = catholic

hre = yes

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1540.1.1 = {
	religion = protestant
}
1660.1.1 = {
	owner = SGO
	controller = SGO
	add_core = SGO
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
