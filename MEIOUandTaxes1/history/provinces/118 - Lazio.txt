# 118 - Lazio-Veterbe

owner = PAP
controller = PAP
add_core = PAP

capital = "Veterbe"
trade_goods = olive
culture = umbrian
religion = catholic

hre = no

base_tax = 6
base_production = 1
base_manpower = 0

is_city = yes
temple = yes
town_hall = yes
local_fortification_1 = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1309.1.1 = {
	add_core = PA2
	owner = PA2
	controller = PA2
}
1378.1.1 = {
	remove_core = PA2
	owner = PAP
	controller = PAP
}
1460.1.1 = {
	trade_goods = alum
} # Monts de Tolfa
1520.5.5 = {
	base_tax = 8
	base_production = 1
	base_manpower = 0
	fort_14th = yes
}
1530.1.1 = {
	road_network = no
	paved_road_network = yes
}
1750.1.1 = {
	add_core = ITA
}
1809.4.10 = {
	controller = FRA
} # Occupied by French troops
1809.10.14 = {
	owner = FRA
	add_core = FRA
} # Treaty of Schönbrunn
1814.4.11 = {
	owner = PAP
	controller = PAP
	remove_core = FRA
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1870.1.1 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
