# 75 - s'Elsass

owner = STR
controller = STR
add_core = STR

capital = "Z�were"
trade_goods = wine
culture = rhine_alemanisch
religion = catholic

hre = yes

base_tax = 5
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
town_hall = yes
workshop = yes
marketplace = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 6
	base_production = 1
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1648.10.24 = {
	add_core = FRA
	owner = FRA
	controller = FRA
}
1697.9.20 = {
	hre = no
}
