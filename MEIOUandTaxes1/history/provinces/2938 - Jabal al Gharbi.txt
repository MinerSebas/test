# 2938 - Sabha

owner = FZA
controller = FZA
add_core = FZA

capital = "Sabha"
trade_goods = livestock
culture = libyan
religion = sunni

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 4.5
native_hostileness = 9

discovered_by = muslim
discovered_by = soudantech

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 77 }
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1551.1.1 = {
	owner = TRP
	controller = TRP
	add_core = TRP
} # Conquered by Ottoman troops
1711.1.1 = {
	owner = TRP
	controller = TRP
	add_core = TRP
	remove_core = TUR
} # The Karamanli pashas gain some autonomy as the Ottoman central power weakens
