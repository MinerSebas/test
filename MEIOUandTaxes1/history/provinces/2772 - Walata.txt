# 2772 - Walata

owner = MAL
controller = MAL
add_core = MAL

capital = "Walata"
trade_goods = livestock
culture = soninke
religion = sunni

hre = no

base_tax = 1
base_production = 2
base_manpower = 0

is_city = yes
marketplace = yes
town_hall = yes
local_fortification_1 = yes

discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 58 }
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1356.1.1 = {
	add_core = GHA
}