# 3734 - Hohenlohe

owner = HHL
controller = HHL
add_core = HHL

capital = "�hringen"
trade_goods = wine
culture = eastfranconian
religion = catholic

hre = yes

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1551.1.1 = {
	religion = protestant
}
1806.7.12 = {
	hre = no
	owner = WUR
	controller = WUR
	add_core = WUR
} # The Holy Roman Empire is dissolved
