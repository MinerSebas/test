#51 - Kleve

owner = KLE
controller = KLE
add_core = KLE

capital = "Kleve"
trade_goods = hemp
culture = dutch
religion = catholic

hre = yes

base_tax = 5
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
workshop = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 6
	base_production = 1
	base_manpower = 0
}
1521.3.15 = {
	owner = JBC
	controller = JBC
	add_core = JBC
	remove_core = KLE
}
1609.1.1 = {
	owner = BRA
	controller = BRA
	add_core = BRA
} # The local line became extinct in the male line in 1609, when Kleve passed to the son-in-law, the elector of Brandenburg.
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king of Prussia
1801.2.9 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Treaty of LunÚville
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1815.6.9 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = FRA
} # Congress of Vienna
