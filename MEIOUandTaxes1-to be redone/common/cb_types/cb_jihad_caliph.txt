cb_jihad_caliph = {
	war_goal = superiority_crusade

	is_triggered_only = yes
	valid_for_subject = no

	prerequisites = {
		has_country_flag = caliph
		religion_group = muslim
		check_variable = {
			which = caliph_authority
			value = 26
		}
		FROM = {
			NOT = { religion = ROOT }
		}
		is_at_war = no
		FROM = {
			NOT = { has_country_flag = caliph } # Handled with cb_rival_caliph
		}
		is_neighbor_of = FROM
		OR = {
			religion_group = muslim
			check_variable = {
				which = caliph_authority
				value = 51
			}
			FROM = {
				any_owned_province = {
					religion = ROOT
				}
			}
		}
		OR = {
			ai = no
			NOT = { has_country_flag = great_predator }
		}
	}
}
