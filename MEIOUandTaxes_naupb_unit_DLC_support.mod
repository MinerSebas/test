name="M&T Native Americans II Unit DLC Support"
path="mod/MEIOUandTaxes_naupb_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesNAN.jpg"
supported_version="1.24.*.*"
