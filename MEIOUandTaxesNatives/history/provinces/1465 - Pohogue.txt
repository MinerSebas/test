# No previous file for Pohogue

culture = shoshone
religion = totemism
capital = "Pohogue"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 3
native_hostileness = 6

1760.1.1  = {	owner = SHO
		controller = SHO
		add_core = SHO
		trade_goods = wool
		is_city = yes } #Great Plain tribes spread over vast territories
