# No previous file for Uitoto

owner = CBS
controller = CBS
add_core = CBS
is_city = yes
culture = muisca
religion = aztec_reformed
capital = "Uitoto"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 1
native_hostileness = 2


1524.1.1   = {  }	#FB was 1524
1537.1.13  = {
	add_core = SPA
	owner = SPA
	controller = SPA
	culture = castillian
	religion = catholic
	is_city = yes
	trade_goods = cacao
	change_province_name = "Popay�n"
	rename_capital = "Popay�n"
}
1750.1.1  = {
	add_core = COL
	culture = colombian
}
1810.7.20  = {
	owner = COL
	controller = COL
} # Colombia declares independence
1819.8.7   = {
	remove_core = SPA
} # Colombia's independence is recongnized

# 1831.11.19 - Grand Colombia is dissolved
