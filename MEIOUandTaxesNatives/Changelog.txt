#################
#   Changelog   #
#################

- 2014.03.24
[*]Added back the Vanilla national ideas for the North American native ntions.
- 2014.07.21
[*]New nations added
- 2015.01.03
[*]Added discoveries for north America natives
- 2015.05.19
[*]Edited the idea groups picked by the AI natives.
- 2016.09.29
[*]Fixes an issue wit colonial triggers for north America native cultures.
